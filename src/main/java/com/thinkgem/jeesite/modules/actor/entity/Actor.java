/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.actor.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 影片演员Entity
 * @author zhl
 * @version 2018-04-16
 */
public class Actor extends DataEntity<Actor> {
	
	private static final long serialVersionUID = 1L;
	private Long movieId;		// 影片ID
	private String realName;		// 真实姓名
	private String actorName;		// 扮演角色名称
	private String icon;		// 演员照片
	private String sex;		// 0 女 、 1男
	private String state;		// -1删除、0下架、1在线
	
	public Actor() {
		super();
	}

	public Actor(String id){
		super(id);
	}

	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	
	@Length(min=0, max=100, message="真实姓名长度必须介于 0 和 100 之间")
	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	@Length(min=0, max=100, message="扮演角色名称长度必须介于 0 和 100 之间")
	public String getActorName() {
		return actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}
	
	@Length(min=0, max=150, message="演员照片长度必须介于 0 和 150 之间")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Length(min=1, max=11, message="0 女 、 1男长度必须介于 1 和 11 之间")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@Length(min=1, max=11, message="-1删除、0下架、1在线长度必须介于 1 和 11 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
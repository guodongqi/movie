/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.actor.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.actor.entity.Actor;
import com.thinkgem.jeesite.modules.actor.dao.ActorDao;

/**
 * 影片演员Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class ActorService extends CrudService<ActorDao, Actor> {

	public Actor get(String id) {
		return super.get(id);
	}
	
	public List<Actor> findList(Actor actor) {
		return super.findList(actor);
	}
	
	public Page<Actor> findPage(Page<Actor> page, Actor actor) {
		return super.findPage(page, actor);
	}
	
	@Transactional(readOnly = false)
	public void save(Actor actor) {
		super.save(actor);
	}
	
	@Transactional(readOnly = false)
	public void delete(Actor actor) {
		super.delete(actor);
	}
	
}
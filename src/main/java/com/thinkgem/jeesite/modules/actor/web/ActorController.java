/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.actor.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.actor.entity.Actor;
import com.thinkgem.jeesite.modules.actor.service.ActorService;

/**
 * 影片演员Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/actor/actor")
public class ActorController extends BaseController {

	@Autowired
	private ActorService actorService;
	
	@ModelAttribute
	public Actor get(@RequestParam(required=false) String id) {
		Actor entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = actorService.get(id);
		}
		if (entity == null){
			entity = new Actor();
		}
		return entity;
	}
	
	@RequiresPermissions("actor:actor:view")
	@RequestMapping(value = {"list", ""})
	public String list(Actor actor, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Actor> page = actorService.findPage(new Page<Actor>(request, response), actor); 
		model.addAttribute("page", page);
		return "modules/actor/actorList";
	}

	@RequiresPermissions("actor:actor:view")
	@RequestMapping(value = "form")
	public String form(Actor actor, Model model) {
		model.addAttribute("actor", actor);
		return "modules/actor/actorForm";
	}

	@RequiresPermissions("actor:actor:edit")
	@RequestMapping(value = "save")
	public String save(Actor actor, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, actor)){
			return form(actor, model);
		}
		actorService.save(actor);
		addMessage(redirectAttributes, "保存zhl成功");
		return "redirect:"+Global.getAdminPath()+"/actor/actor/?repage";
	}
	
	@RequiresPermissions("actor:actor:edit")
	@RequestMapping(value = "delete")
	public String delete(Actor actor, RedirectAttributes redirectAttributes) {
		actorService.delete(actor);
		addMessage(redirectAttributes, "删除zhl成功");
		return "redirect:"+Global.getAdminPath()+"/actor/actor/?repage";
	}

}
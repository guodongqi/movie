/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.api.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.movie.service.MovieService;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import com.thinkgem.jeesite.modules.moviehouse.service.MovieHouseService;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.moviesale.service.MovieSaleService;
import com.thinkgem.jeesite.modules.order.entity.Order;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.Role;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.Constants;
import com.thinkgem.jeesite.modules.sys.utils.JsonUtil;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 影片上映信息Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/api/movie/")
public class MovieApiController extends BaseController {

	@Autowired
	private MovieService movieService;
	@Autowired
	private MovieSaleService movieSaleService;
	@Autowired
	private MovieHouseService movieHouseService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserDao userDao;


	/**
	 * 查询影片列表接口
	 *
	 * @param mvId
	 *         影片Id
	 * @param time
	 *         影片上映时间区间
	 * @param request
	 * @param response
	 *
	 * @return msId  影片上映Id
	 * className  班级名称
	 * officeName 机构名称
	 * beginCreateDate  添加时间
	 * studentNum 班级人数
	 * count
	 * pageNo
	 * pageSize
	 *
	 * @author zhl
	 * @date 2017/11/28
	 */
	@RequestMapping("mvList")
	@ResponseBody
	public JSONObject mvList( @RequestParam(required = false) String mvId,
							  @RequestParam(required = false) String time,
							  @RequestParam(required = false, defaultValue = "1") int pageNo,
							  @RequestParam(required = false, defaultValue = "6") int pageSize,
							  HttpServletRequest request,
							  HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			Movie movie = new Movie();
			Page page = new Page( request, response );
			page.setPageNo( pageNo );
			page.setPageSize( pageSize );
			movie.setPage(page);
			List<Movie> mvList = movieService.findList(movie);
			page.setList(mvList);
			jsonObject.put( "count", page.getCount() );
			jsonObject.put( "pageNo", pageNo );
			jsonObject.put( "pageSize", pageSize );
			for(Movie mv:mvList){
				Map<String, Object> etMap = Maps.newHashMap();
				if(mv!=null) {
					etMap.put("movieId", mv.getId());
					etMap.put("movieName", mv.getName());
					etMap.put("movieLevel", mv.getLevel());
					etMap.put("movieTime", mv.getTime());
					etMap.put("movieType", mv.getType());
					etMap.put("description", mv.getDescription());
					etMap.put("movieBannerUrl", mv.getBannerUrl());
					etMap.put("movieAdvanceUrl", mv.getAdvanceUrl());
					etMap.put("movieActors", mv.getActors());
					etMap.put("price", mv.getPrice());
				}
				data.add( etMap );
			}
			if ( this.isPageOut( page.getCount(), pageNo, pageSize ))
				data = Lists.newArrayList();
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}
	
	
	@RequestMapping("mvRecommendList")
	@ResponseBody
	public JSONObject mvRecommendList( @RequestParam(required = false) String mvId,
							  @RequestParam(required = false) String time,
							  @RequestParam(required = false, defaultValue = "1") int pageNo,
							  @RequestParam(required = false, defaultValue = "6") int pageSize,
							  HttpServletRequest request,
							  HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();

		String type = "科幻";   // 这里应该是查询用户历史观影中最多次数的类型 ， 然后根据这个类型去查询上映的影片  推荐给用户
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			Movie movie = new Movie();
			movie.setType(type);
			Page page = new Page( request, response );
			page.setPageNo( pageNo );
			page.setPageSize( pageSize );
			movie.setPage(page);
			List<Movie> mvList = movieService.findList(movie);
			page.setList(mvList);
			jsonObject.put( "count", page.getCount() );
			jsonObject.put( "pageNo", pageNo );
			jsonObject.put( "pageSize", pageSize );
			for(Movie mv:mvList){
				Map<String, Object> etMap = Maps.newHashMap();
				if(mv!=null) {
					etMap.put("movieId", mv.getId());
					etMap.put("movieName", mv.getName());
					etMap.put("movieLevel", mv.getLevel());
					etMap.put("movieTime", mv.getTime());
					etMap.put("movieType", mv.getType());
					etMap.put("description", mv.getDescription());
					etMap.put("movieBannerUrl", mv.getBannerUrl());
					etMap.put("movieAdvanceUrl", mv.getAdvanceUrl());
					etMap.put("movieActors", mv.getActors());
					etMap.put("price", mv.getPrice());
				}
				data.add( etMap );
			}
			if ( this.isPageOut( page.getCount(), pageNo, pageSize ))
				data = Lists.newArrayList();
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}
	
	
	
	

	/**
	 * @param count    数据总条数
	 * @param pageNo   当前页码
	 * @param pageSize 每页条数
	 * @return
	 * @discription 判断当前页码是否超出最大页数
	 * @author pwj
	 * @date 2016/9/27 17:35
	 */
	public static boolean isPageOut( long count, int pageNo, int pageSize ) {
		if ( pageSize == 0 || count == 0 || pageNo == 0 )
			return true;
		long lastpage = count / pageSize;
		if ( count % pageSize != 0 )
			lastpage = lastpage + 1;
		if ( pageNo > lastpage )
			return true;
		else
			return false;
	}

	/**
	 * 查询上映信息列表接口
	 *
	 * @param mvId
	 *         影片Id
	 * @param time
	 *         影片上映时间区间
	 * @param request
	 * @param response
	 *
	 * @return msId  影片上映Id
	 * className  班级名称
	 * officeName 机构名称
	 * beginCreateDate  添加时间
	 * studentNum 班级人数
	 * count
	 * pageNo
	 * pageSize
	 *
	 * @author zhl
	 * @date 2017/11/28
	 */
	@RequestMapping("msList")
	@ResponseBody
	public JSONObject msList( @RequestParam(required = false) String mvId,
							  @RequestParam(required = false) String time,
							  HttpServletRequest request,
							  HttpServletResponse response ) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			if(StringUtils.isNotBlank(mvId)){
				MovieSale movieSale = new MovieSale();
				movieSale.setMovie(new Movie());
				movieSale.getMovie().setId(mvId);
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
				Date today = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(today);
				if(StringUtils.isNotBlank(time)) {
					c.add(Calendar.DAY_OF_MONTH, Integer.parseInt(time));// 今天+1天
					Date timeRange = f.parse(f.format(c.getTime()));
					movieSale.setTimeRange(timeRange);
				}
				List<MovieSale> msList = movieSaleService.findMoviceSaleList(movieSale);
                //设置要获取到什么样的时间
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				for(MovieSale ms:msList){
					Map<String, Object> etMap = Maps.newHashMap();
					if(ms!=null) {
						etMap.put("msId", ms.getId());
						etMap.put("createTime", sdf.format(ms.getCreateTime()));
						etMap.put("startTime", sdf.format(ms.getStartTime()));
						etMap.put("endTime", sdf.format(ms.getEndTime()));
						etMap.put("langueVersion", ms.getLangueVersion());
						if(ms.getMovieHouse()!=null){
							etMap.put("movieHouse", ms.getMovieHouse().getName());
						}
						etMap.put("currentPrice", ms.getCurrentPrice());
					}
					data.add( etMap );
				}
			}
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}

	@RequestMapping(value = "msDetail")
	public String msDetail(MovieSale movieSale, Model model) {
		if(movieSale==null){
			movieSale = new MovieSale();
		}
		movieSale = movieSaleService.getMoviceSale(movieSale);
		
		model.addAttribute("movieSale", movieSale);
		return "modules/front/select_site";
	}
	
	/*
	 * 
	 * 跳转到推荐页面
	 * */
	@RequestMapping(value = "msRecommend")
	public String msRecommend() {
		return "modules/front/recommend_movie";
	}
	
	@RequestMapping("msSiteInfo")
	@ResponseBody
	public JSONObject msSiteInfo( @RequestParam(required = false) String msId,
							  HttpServletRequest request,
							  HttpServletResponse response ) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		JSONObject data = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		try {
			if(StringUtils.isNotBlank(msId)){
				MovieSale movieSale = movieSaleService.get(msId);
				Movie movie = movieService.get(movieSale.getMovieId().toString());
				MovieHouse mh = movieHouseService.get(movieSale.getMovieHouseId().toString());
				data.put("movieName", movie.getName());
				data.put("movieId", movie.getId());
				data.put("movieType", movie.getType());
				data.put("movieSaleId", movieSale.getId());
				data.put("bannerUrl", movie.getBannerUrl());
				data.put("house", mh.getName());
				data.put("startTime", sdf.format(movieSale.getStartTime()));
				data.put("alreadyJoinSite", movieSale.getAlreadyJoinSite());
				data.put("price", movieSale.getCurrentPrice());
				data.put("time", movie.getTime());
			}
			JsonUtil.addRightData( jsonObject, "", data);
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}
	
	/**
	 * 跳转到关于我们
	 * @param order
	 * @param model
	 * @return
	 */
	@RequestMapping("aboutUs")
	public String aboutUs(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/front/about_us";
	}
	

	@RequestMapping("checkUser")
	@ResponseBody
	public JSONObject checkUser( HttpServletRequest request,
								  HttpServletResponse response ) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		JSONObject data = new JSONObject();
		try {
			User user = UserUtils.getUser();
			if(StringUtils.isBlank(user.getId())){
				throw new RuntimeException( "没有登陆" );
			}else{
				JsonUtil.addRightData( jsonObject, "已登陆", data);
			}
		} catch ( JSONException je ) {
		je.printStackTrace();
		String message = je.getMessage();
		jsonObject = JsonUtil.addFialMessage( new JSONObject(), org.apache.commons.lang.StringUtils.isNotBlank( message ) ? message : "json转换异常" );
		} catch ( RuntimeException re ) {
			re.printStackTrace();
			String message = re.getMessage();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), org.apache.commons.lang.StringUtils.isNotBlank( message ) ? message : "未知的运行时异常" );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}

	@RequestMapping(value = "registerUser")
	public String registerUser(User user, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		return "modules/front/register";
	}

	/**
	 * 验证登录名是否有效
	 * @param oldLoginName
	 * @param loginName
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "checkLoginName")
	public String checkLoginName(String oldLoginName, String loginName) {
		if (loginName !=null && loginName.equals(oldLoginName)) {
			return "true";
		} else if (loginName !=null && systemService.getUserByLoginName(loginName) == null) {
			return "true";
		}
		return "false";
	}

	@RequestMapping(value = "saveUser")
	public String saveUser(User user, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		// 如果新密码为空，则不更换密码
		if (StringUtils.isNotBlank(user.getPassword())) {
			user.setPassword(SystemService.entryptPassword(user.getPassword()));
		}
		user.setCompany(new Office("1"));
		user.setOffice(new Office("1"));
		user.setId(IdGen.uuid());
		User user1= new User();
		user1.setId("1");
		user.setCreateBy(user1);
		user.setUpdateBy(user1);
		Date date = new Date();
		user.setCreateDate(date);
		user.setUpdateDate(date);
		userDao.insert(user);
		addMessage(redirectAttributes, "保存用户'" + user.getLoginName() + "'成功");
		return "modules/front/movie_index";
	}
}
/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.movie.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.movie.entity.Movie;

/**
 * 影片DAO接口
 * @author zhl
 * @version 2018-04-16
 */
@MyBatisDao
public interface MovieDao extends CrudDao<Movie> {
	
}
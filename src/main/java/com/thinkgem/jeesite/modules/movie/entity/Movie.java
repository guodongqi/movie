/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.movie.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 影片Entity
 * @author zhl
 * @version 2018-04-16
 */
public class Movie extends DataEntity<Movie> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 影片名称
	private String price;		// 影片价格 分为单位
	private String description;		// 描述
	private String bannerUrl;		// 头图
	private String advanceUrl;		// 预告片地址
	private String level;		// 评分
	private String time;		// 时长  分钟单位
	private String state;		// 0 下架  、-1删除	、1正常在售
	private String type;        //类型
	private String actors;	// 演员

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Movie() {
		super();
	}

	public Movie(String id){
		super(id);
	}

	@Length(min=0, max=50, message="影片名称长度必须介于 0 和 50 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=11, message="影片价格 分为单位长度必须介于 0 和 11 之间")
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Length(min=0, max=255, message="描述长度必须介于 0 和 255 之间")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Length(min=0, max=500, message="头图长度必须介于 0 和 500 之间")
	public String getBannerUrl() {
		return bannerUrl;
	}

	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}
	
	@Length(min=0, max=255, message="预告片地址长度必须介于 0 和 255 之间")
	public String getAdvanceUrl() {
		return advanceUrl;
	}

	public void setAdvanceUrl(String advanceUrl) {
		this.advanceUrl = advanceUrl;
	}
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	@Length(min=0, max=11, message="时长  分钟单位长度必须介于 0 和 11 之间")
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	@Length(min=1, max=11, message="0 下架  、-1删除	、1正常在售长度必须介于 1 和 11 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
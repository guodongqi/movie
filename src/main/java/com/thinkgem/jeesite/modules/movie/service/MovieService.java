/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.movie.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.movie.dao.MovieDao;

/**
 * 影片Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class MovieService extends CrudService<MovieDao, Movie> {

	public Movie get(String id) {
		return super.get(id);
	}
	
	public List<Movie> findList(Movie movie) {
		return super.findList(movie);
	}
	
	public Page<Movie> findPage(Page<Movie> page, Movie movie) {
		return super.findPage(page, movie);
	}
	
	@Transactional(readOnly = false)
	public void save(Movie movie) {
		super.save(movie);
	}
	
	@Transactional(readOnly = false)
	public void delete(Movie movie) {
		super.delete(movie);
	}
	
}
/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.movie.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.sys.utils.Constants;
import com.thinkgem.jeesite.modules.sys.utils.JsonUtil;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.movie.service.MovieService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 影片Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/movie/movie")
public class MovieController extends BaseController {

	@Autowired
	private MovieService movieService;
	
	@ModelAttribute
	public Movie get(@RequestParam(required=false) String id) {
		Movie entity = null;

		if (StringUtils.isNotBlank(id)){
			entity = movieService.get(id);
		}
		if (entity == null){
			entity = new Movie();
		}
		return entity;
	}
	
	@RequiresPermissions("movie:movie:view")
	@RequestMapping(value = {"list", ""})
	public String list(Movie movie, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Movie> page = movieService.findPage(new Page<Movie>(request, response), movie);

		System.out.println("git TEST");
		model.addAttribute("page", page);
		return "modules/movie/movieList";
	}

	@RequiresPermissions("movie:movie:view")
	@RequestMapping(value = "form")
	public String form(Movie movie, Model model) {
		model.addAttribute("movie", movie);
		return "modules/movie/movieForm";
	}

	@RequiresPermissions("movie:movie:edit")
	@RequestMapping(value = "save")
	public String save(Movie movie, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, movie)){
			return form(movie, model);
		}
		movieService.save(movie);
		addMessage(redirectAttributes, "保存影片成功");
		return "redirect:"+Global.getAdminPath()+"/movie/movie/?repage";
	}
	
	@RequiresPermissions("movie:movie:edit")
	@RequestMapping(value = "delete")
	public String delete(Movie movie, RedirectAttributes redirectAttributes) {
		movieService.delete(movie);
		addMessage(redirectAttributes, "删除影片成功");
		return "redirect:"+Global.getAdminPath()+"/movie/movie/?repage";
	}

	/**
	 * 查询影片列表接口
	 *
	 * @param mvId
	 *         影片Id
	 * @param time
	 *         影片上映时间区间
	 * @param request
	 * @param response
	 *
	 * @return msId  影片上映Id
	 * className  班级名称
	 * officeName 机构名称
	 * beginCreateDate  添加时间
	 * studentNum 班级人数
	 * count
	 * pageNo
	 * pageSize
	 *
	 * @author zhl
	 * @date 2017/11/28
	 */
	@RequestMapping("msList")
	@ResponseBody
	public JSONObject msList( @RequestParam(required = false) String mvId,
							  @RequestParam(required = false) String time,
							  HttpServletRequest request,
							  HttpServletResponse response ) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			Movie movie = new Movie();
			List<Movie> mvList = movieService.findList(movie);
			for(Movie mv:mvList){
				Map<String, Object> etMap = Maps.newHashMap();
				if(mv!=null) {
					etMap.put("movieId", mv.getId());
					etMap.put("movieName", mv.getName());
					etMap.put("movieLevel", mv.getLevel());
					etMap.put("movieTime", mv.getTime());
					etMap.put("movieType", mv.getType());
					etMap.put("description", mv.getDescription());
				}
				data.add( etMap );
			}
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}


	/**
	 * 查询影片信息接口
	 *
	 * @param mvId
	 *         影片Id
	 * @param request
	 * @param response
	 *
	 * @return msId  影片上映Id
	 * className  班级名称
	 * officeName 机构名称
	 * beginCreateDate  添加时间
	 * studentNum 班级人数
	 * count
	 * pageNo
	 * pageSize
	 *
	 * @author zhl
	 * @date 2017/11/28
	 */
	@RequestMapping("mvDetail")
	@ResponseBody
	public JSONObject mvDetail(@RequestParam(required = false) String mvId,
							   HttpServletRequest request,
							   HttpServletResponse response ) {
		JSONObject jsonObject = new JSONObject();

		//如果传进来的用户Id为空就不用查了
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			if(StringUtils.isNotBlank(mvId)){
				Movie movie = new Movie();
				movie.setId(mvId);
				movie = movieService.get(mvId);
				Map<String, Object> etMap = Maps.newHashMap();
				if(movie!=null) {
					etMap.put("movieId", movie.getId());
					etMap.put("movieName", movie.getName());
					etMap.put("movieLevel", movie.getLevel());
					etMap.put("movieTime", movie.getTime());
					etMap.put("movieType", movie.getType());
					etMap.put("description", movie.getDescription());
				}
				data.add( etMap );
			}
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}

	/**
	 * zhl:获取影片信息
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "findMovieList")
	public List<Map<String, Object>> findMovieList(Movie movie , HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Movie> movieList = movieService.findList(movie);
		StringBuilder queryStr = new StringBuilder();
		for (int i=0; i<movieList.size(); i++){
			Movie mv = movieList.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", mv.getId());
			map.put("name", mv.getName());
			mapList.add(map);
		}
		return mapList;
	}

	/**
	 * pwj:自己定义的标签
	 * @param request
	 * @param model
	 * @return
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "treeselect")
	public String treeselect(HttpServletRequest request, Model model) {
		model.addAttribute("url", request.getParameter("url")); 	// 树结构数据URL
		model.addAttribute("type",request.getParameter("type"));
		model.addAttribute("extId", request.getParameter("extId")); // 排除的编号ID
		model.addAttribute("checked", request.getParameter("checked")); // 是否可复选
		model.addAttribute("selectIds", request.getParameter("selectIds")); // 指定默认选中的ID
		model.addAttribute("isAll", request.getParameter("isAll")); 	// 是否读取全部数据，不进行权限过滤
		model.addAttribute("module", request.getParameter("module"));	// 过滤栏目模型（仅针对CMS的Category树）
		return "modules/movie/tagTreeSelectBack";
	}
}
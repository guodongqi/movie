/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviehouse.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 影片信息Entity
 * @author zhl
 * @version 2018-04-16
 */
public class MovieHouse extends DataEntity<MovieHouse> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 影厅名称
	private String number;		// 编号
	private String maxJoin;		// 最大容纳人数
	private String state;		// -1删除 、1正常、0关停
	private String siteInfo;		// 座位信息
	private Date createTime;		// create_time
	
	public MovieHouse() {
		super();
	}

	public MovieHouse(String id){
		super(id);
	}

	@Length(min=0, max=255, message="影厅名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=20, message="编号长度必须介于 0 和 20 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=11, message="最大容纳人数长度必须介于 0 和 11 之间")
	public String getMaxJoin() {
		return maxJoin;
	}

	public void setMaxJoin(String maxJoin) {
		this.maxJoin = maxJoin;
	}
	
	@Length(min=1, max=11, message="-1删除 、1正常、0关停长度必须介于 1 和 11 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Length(min=0, max=10000, message="作为信息长度必须介于 0 和 500 之间")
	public String getSiteInfo() {
		return siteInfo;
	}

	public void setSiteInfo(String siteInfo) {
		this.siteInfo = siteInfo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="create_time不能为空")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
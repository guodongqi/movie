/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviehouse.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import com.thinkgem.jeesite.modules.moviehouse.dao.MovieHouseDao;

/**
 * 影片信息Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class MovieHouseService extends CrudService<MovieHouseDao, MovieHouse> {

	public MovieHouse get(String id) {
		return super.get(id);
	}
	
	public List<MovieHouse> findList(MovieHouse movieHouse) {
		return super.findList(movieHouse);
	}
	
	public Page<MovieHouse> findPage(Page<MovieHouse> page, MovieHouse movieHouse) {
		return super.findPage(page, movieHouse);
	}
	
	@Transactional(readOnly = false)
	public void save(MovieHouse movieHouse) {
		super.save(movieHouse);
	}
	
	@Transactional(readOnly = false)
	public void delete(MovieHouse movieHouse) {
		super.delete(movieHouse);
	}
	
}
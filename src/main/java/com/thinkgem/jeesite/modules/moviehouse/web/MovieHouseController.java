/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviehouse.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.modules.movie.entity.Movie;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import com.thinkgem.jeesite.modules.moviehouse.service.MovieHouseService;

import java.util.List;
import java.util.Map;

/**
 * 影片信息Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/moviehouse/movieHouse")
public class MovieHouseController extends BaseController {

	@Autowired
	private MovieHouseService movieHouseService;
	
	@ModelAttribute
	public MovieHouse get(@RequestParam(required=false) String id) {
		MovieHouse entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = movieHouseService.get(id);
		}
		if (entity == null){
			entity = new MovieHouse();
		}
		return entity;
	}
	
	@RequiresPermissions("moviehouse:movieHouse:view")
	@RequestMapping(value = {"list", ""})
	public String list(MovieHouse movieHouse, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MovieHouse> page = movieHouseService.findPage(new Page<MovieHouse>(request, response), movieHouse); 
		model.addAttribute("page", page);
		return "modules/moviehouse/movieHouseList";
	}

	@RequiresPermissions("moviehouse:movieHouse:view")
	@RequestMapping(value = "form")
	public String form(MovieHouse movieHouse, Model model) {
		model.addAttribute("movieHouse", movieHouse);
		return "modules/moviehouse/movieHouseForm";
	}

	@RequiresPermissions("moviehouse:movieHouse:edit")
	@RequestMapping(value = "save")
	public String save(MovieHouse movieHouse, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, movieHouse)){
			return form(movieHouse, model);
		}
		movieHouseService.save(movieHouse);
		addMessage(redirectAttributes, "保存影片信息成功");
		return "redirect:"+Global.getAdminPath()+"/moviehouse/movieHouse/?repage";
	}
	
	@RequiresPermissions("moviehouse:movieHouse:edit")
	@RequestMapping(value = "delete")
	public String delete(MovieHouse movieHouse, RedirectAttributes redirectAttributes) {
		movieHouseService.delete(movieHouse);
		addMessage(redirectAttributes, "删除影片信息成功");
		return "redirect:"+Global.getAdminPath()+"/moviehouse/movieHouse/?repage";
	}

	/**
	 * zhl:获取影厅信息
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "findMovieHouseList")
	public List<Map<String, Object>> findMovieHouseList(MovieHouse movieHouse , HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<MovieHouse> movieList = movieHouseService.findList(movieHouse);
		StringBuilder queryStr = new StringBuilder();
		for (int i=0; i<movieList.size(); i++){
			MovieHouse mh = movieList.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", mh.getId());
			map.put("name", mh.getName());
			mapList.add(map);
		}
		return mapList;
	}


}
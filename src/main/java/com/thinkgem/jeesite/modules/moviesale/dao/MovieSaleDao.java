/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviesale.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;

import java.util.List;

/**
 * 影片上映信息DAO接口
 * @author zhl
 * @version 2018-04-16
 */
@MyBatisDao
public interface MovieSaleDao extends CrudDao<MovieSale> {

    /**
     * 查询上映信息接口
     */
    MovieSale getMoviceSale(MovieSale movieSale);

    /**
     * 查询上映信息列表
     */
    List<MovieSale> findMoviceSaleList(MovieSale movieSale);
}


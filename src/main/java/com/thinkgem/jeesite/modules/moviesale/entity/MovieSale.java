/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviesale.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 影片上映信息Entity
 * @author zhl
 * @version 2018-04-16
 */
public class MovieSale extends DataEntity<MovieSale> {
	
	private static final long serialVersionUID = 1L;
	private Long movieHouseId;		// 影厅ID
	private Long movieId;		// 影片ID
	private Date startTime;		// 开始时间
	private Date endTime;		// 结束时间
	private String alreadyJoin;		// 当前参加人数
	private String alreadyJoinSite;		// 当前应预定的二位数组
	private String currentPrice;		// 单位分，当前售卖的价格
	private Date createTime;		// create_time
	private String state;		// -1删除、0下架、1在线
	private Movie movie;    //影片

	public MovieHouse getMovieHouse() {
		return movieHouse;
	}

	public void setMovieHouse(MovieHouse movieHouse) {
		this.movieHouse = movieHouse;
	}

	private MovieHouse movieHouse;  //影厅
    private String langueVersion;   //语言版本

	public Date getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(Date timeRange) {
		this.timeRange = timeRange;
	}

	private Date timeRange;         //时间区间

	public String getLangueVersion() {
		return langueVersion;
	}

	public void setLangueVersion(String langueVersion) {
		this.langueVersion = langueVersion;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public MovieSale() {
		super();
	}

	public MovieSale(String id){
		super(id);
	}

	public Long getMovieHouseId() {
		return movieHouseId;
	}

	public void setMovieHouseId(Long movieHouseId) {
		this.movieHouseId = movieHouseId;
	}
	
	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="开始时间不能为空")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="结束时间不能为空")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@Length(min=1, max=11, message="当前参加人数长度必须介于 1 和 11 之间")
	public String getAlreadyJoin() {
		return alreadyJoin;
	}

	public void setAlreadyJoin(String alreadyJoin) {
		this.alreadyJoin = alreadyJoin;
	}
	
	@Length(min=0, max=255, message="当前应预定的二位数组长度必须介于 0 和 255 之间")
	public String getAlreadyJoinSite() {
		return alreadyJoinSite;
	}

	public void setAlreadyJoinSite(String alreadyJoinSite) {
		this.alreadyJoinSite = alreadyJoinSite;
	}
	
	@Length(min=0, max=11, message="单位分，当前售卖的价格长度必须介于 0 和 11 之间")
	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="create_time不能为空")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Length(min=1, max=11, message="-1删除、0下架、1在线长度必须介于 1 和 11 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
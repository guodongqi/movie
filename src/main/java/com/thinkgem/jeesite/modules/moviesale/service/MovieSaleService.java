/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviesale.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.moviesale.dao.MovieSaleDao;

/**
 * 影片上映信息Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class MovieSaleService extends CrudService<MovieSaleDao, MovieSale> {

	public MovieSale get(String id) {
		return super.get(id);
	}
	
	public List<MovieSale> findList(MovieSale movieSale) {
		return super.findList(movieSale);
	}
	
	public Page<MovieSale> findPage(Page<MovieSale> page, MovieSale movieSale) {
		return super.findPage(page, movieSale);
	}
	
	@Transactional(readOnly = false)
	public void save(MovieSale movieSale) {
		super.save(movieSale);
	}
	
	@Transactional(readOnly = false)
	public void delete(MovieSale movieSale) {
		super.delete(movieSale);
	}


	/**
	 * 查询上映信息接口
	 * */
	@Transactional(readOnly = false)
	public MovieSale getMoviceSale(MovieSale movieSale) {
		movieSale = dao.getMoviceSale(movieSale);
		return movieSale;
	}

	/**
	 * 查询上映信息接口
	 * */
	@Transactional(readOnly = false)
	public List<MovieSale> findMoviceSaleList(MovieSale movieSale) {
		return dao.findMoviceSaleList(movieSale);
	}
}
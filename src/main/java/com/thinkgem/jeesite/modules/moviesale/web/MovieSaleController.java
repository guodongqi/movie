/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.moviesale.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import com.thinkgem.jeesite.modules.moviehouse.service.MovieHouseService;
import com.thinkgem.jeesite.modules.sys.utils.Constants;
import com.thinkgem.jeesite.modules.sys.utils.JsonUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.moviesale.service.MovieSaleService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 影片上映信息Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/moviesale/movieSale")
public class MovieSaleController extends BaseController {

	@Autowired
	private MovieSaleService movieSaleService;
	@Autowired
	private MovieHouseService movieHouseService;
	
	@ModelAttribute
	public MovieSale get(@RequestParam(required=false) String id) {
		MovieSale entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = movieSaleService.get(id);
		}
		if (entity == null){
			entity = new MovieSale();
		}
		return entity;
	}
	
	@RequiresPermissions("moviesale:movieSale:view")
	@RequestMapping(value = {"list", ""})
	public String list(MovieSale movieSale, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MovieSale> page = movieSaleService.findPage(new Page<MovieSale>(request, response), movieSale); 
		model.addAttribute("page", page);
		return "modules/moviesale/movieSaleList";
	}

	@RequiresPermissions("moviesale:movieSale:view")
	@RequestMapping(value = "form")
	public String form(MovieSale movieSale, Model model) {
		model.addAttribute("movieSale", movieSale);
		return "modules/moviesale/movieSaleForm";
	}

	@RequiresPermissions("moviesale:movieSale:edit")
	@RequestMapping(value = "save")
	public String save(MovieSale movieSale, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, movieSale)){
			return form(movieSale, model);
		}
        if(movieSale.getMovieHouse()!=null){
			MovieHouse movieHouse = movieHouseService.get(movieSale.getMovieHouseId()+"");
			movieSale.setAlreadyJoinSite(movieHouse.getSiteInfo());
		}
		movieSale.setAlreadyJoin("0");
		movieSaleService.save(movieSale);
		addMessage(redirectAttributes, "保存影片上映信息成功");
		return "redirect:"+Global.getAdminPath()+"/moviesale/movieSale/?repage";
	}
	
	@RequiresPermissions("moviesale:movieSale:edit")
	@RequestMapping(value = "delete")
	public String delete(MovieSale movieSale, RedirectAttributes redirectAttributes) {
		movieSaleService.delete(movieSale);
		addMessage(redirectAttributes, "删除影片上映信息成功");
		return "redirect:"+Global.getAdminPath()+"/moviesale/movieSale/?repage";
	}
}
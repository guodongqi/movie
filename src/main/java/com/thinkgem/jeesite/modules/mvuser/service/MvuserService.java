/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mvuser.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mvuser.entity.Mvuser;
import com.thinkgem.jeesite.modules.mvuser.dao.MvuserDao;

/**
 * 影院用户Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class MvuserService extends CrudService<MvuserDao, Mvuser> {

	public Mvuser get(String id) {
		return super.get(id);
	}
	
	public List<Mvuser> findList(Mvuser mvuser) {
		return super.findList(mvuser);
	}
	
	public Page<Mvuser> findPage(Page<Mvuser> page, Mvuser mvuser) {
		return super.findPage(page, mvuser);
	}
	
	@Transactional(readOnly = false)
	public void save(Mvuser mvuser) {
		super.save(mvuser);
	}
	
	@Transactional(readOnly = false)
	public void delete(Mvuser mvuser) {
		super.delete(mvuser);
	}
	
}
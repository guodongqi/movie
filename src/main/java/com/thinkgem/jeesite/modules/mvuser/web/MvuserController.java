/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mvuser.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mvuser.entity.Mvuser;
import com.thinkgem.jeesite.modules.mvuser.service.MvuserService;

/**
 * 影院用户Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/mvuser/mvuser")
public class MvuserController extends BaseController {

	@Autowired
	private MvuserService mvuserService;
	
	@ModelAttribute
	public Mvuser get(@RequestParam(required=false) String id) {
		Mvuser entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mvuserService.get(id);
		}
		if (entity == null){
			entity = new Mvuser();
		}
		return entity;
	}
	
	@RequiresPermissions("mvuser:mvuser:view")
	@RequestMapping(value = {"list", ""})
	public String list(Mvuser mvuser, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Mvuser> page = mvuserService.findPage(new Page<Mvuser>(request, response), mvuser); 
		model.addAttribute("page", page);
		return "modules/mvuser/mvuserList";
	}

	@RequiresPermissions("mvuser:mvuser:view")
	@RequestMapping(value = "form")
	public String form(Mvuser mvuser, Model model) {
		model.addAttribute("mvuser", mvuser);
		return "modules/mvuser/mvuserForm";
	}

	@RequiresPermissions("mvuser:mvuser:edit")
	@RequestMapping(value = "save")
	public String save(Mvuser mvuser, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, mvuser)){
			return form(mvuser, model);
		}
		mvuserService.save(mvuser);
		addMessage(redirectAttributes, "保存影院用户成功");
		return "redirect:"+Global.getAdminPath()+"/mvuser/mvuser/?repage";
	}
	
	@RequiresPermissions("mvuser:mvuser:edit")
	@RequestMapping(value = "delete")
	public String delete(Mvuser mvuser, RedirectAttributes redirectAttributes) {
		mvuserService.delete(mvuser);
		addMessage(redirectAttributes, "删除影院用户成功");
		return "redirect:"+Global.getAdminPath()+"/mvuser/mvuser/?repage";
	}

}
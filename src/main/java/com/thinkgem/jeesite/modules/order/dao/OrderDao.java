/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.order.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.order.entity.Order;

/**
 * 订单DAO接口
 * @author zhl
 * @version 2018-04-16
 */
@MyBatisDao
public interface OrderDao extends CrudDao<Order> {
	
}
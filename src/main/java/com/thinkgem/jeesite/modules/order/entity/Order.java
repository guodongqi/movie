/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.order.entity;

import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.moviehouse.entity.MovieHouse;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 订单Entity
 * @author zhl
 * @version 2018-04-16
 */
public class Order extends DataEntity<Order> {
	
	private static final long serialVersionUID = 1L;
	private String orderNo;		// order_no
	private String totalPrice;		// total_price
	private Long movieSaleId;		// 关联影片的ID
	private String ticketAmount;		// 票的数量
	private String siglePrice;		// 单张票的价格
	private String state;		// state
	private User user;		// 用户ID
	private Date createTime;		// create_time
	private String status;		// 1等待支付、2已付款、3待使用、4待评价、5已完成、0支付失败
	private Movie movie;    //影片
	private MovieSale movieSale; //上映信息
	private MovieHouse movieHouse;
	private String seatDataReal;    //选座信息显示位置
	
	public String getSeatDataReal() {
		return seatDataReal;
	}
	public void setSeatDataReal(String seatDataReal) {
		this.seatDataReal = seatDataReal;
	}
	public String getSeatData() {
		return seatData;
	}
	public void setSeatData(String seatData) {
		this.seatData = seatData;
	}
	private String seatData;    //选座信息

	public MovieHouse getMovieHouse() {
		return movieHouse;
	}

	public void setMovieHouse(MovieHouse movieHouse) {
		this.movieHouse = movieHouse;
	}

	public MovieSale getMovieSale() {
		return movieSale;
	}

	public void setMovieSale(MovieSale movieSale) {
		this.movieSale = movieSale;
	}
	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}


	public Order() {
		super();
	}

	public Order(String id){
		super(id);
	}

	@Length(min=0, max=50, message="order_no长度必须介于 0 和 50 之间")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@Length(min=0, max=11, message="total_price长度必须介于 0 和 11 之间")
	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public Long getMovieSaleId() {
		return movieSaleId;
	}

	public void setMovieSaleId(Long movieSaleId) {
		this.movieSaleId = movieSaleId;
	}
	
	@Length(min=0, max=11, message="票的数量长度必须介于 0 和 11 之间")
	public String getTicketAmount() {
		return ticketAmount;
	}

	public void setTicketAmount(String ticketAmount) {
		this.ticketAmount = ticketAmount;
	}
	
	@Length(min=0, max=11, message="单张票的价格长度必须介于 0 和 11 之间")
	public String getSiglePrice() {
		return siglePrice;
	}

	public void setSiglePrice(String siglePrice) {
		this.siglePrice = siglePrice;
	}
	
	@Length(min=1, max=11, message="state长度必须介于 1 和 11 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="create_time不能为空")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Length(min=1, max=11, message="1等待支付、2已付款、3待使用、4待评价、5已完成、0支付失败长度必须介于 1 和 11 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
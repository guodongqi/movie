/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.order.service;

import java.util.Date;
import java.util.List;

import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.moviesale.service.MovieSaleService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.order.entity.Order;
import com.thinkgem.jeesite.modules.order.dao.OrderDao;

/**
 * 订单Service
 * @author zhl
 * @version 2018-04-16
 */
@Service
@Transactional(readOnly = true)
public class OrderService extends CrudService<OrderDao, Order> {

	@Autowired
	private MovieSaleService movieSaleService;

	public Order get(String id) {
		return super.get(id);
	}
	
	public List<Order> findList(Order order) {
		return super.findList(order);
	}
	
	public Page<Order> findPage(Page<Order> page, Order order) {
		return super.findPage(page, order);
	}
	
	@Transactional(readOnly = false)
	public void save(Order order) {
		super.save(order);
	}
	
	@Transactional(readOnly = false)
	public void delete(Order order) {
		super.delete(order);
	}

	/**
	 * 生成订单
	 * @param order
	 * @return
	 */
	@Transactional(readOnly = false)
	public String createOrder(Order order) {
		String result="1";
		if(order.getSeatData()!=null) {
			order.setStatus("1");
			System.out.println(UserUtils.getUser().getId());
			order.setUser(UserUtils.getUser());
			order.setCreateTime(new Date());
			order.setOrderNo(IdGen.randomLong() + "");
			//页面选择的座位信息
			JSONArray jsonSeat = JSONArray.fromObject(order.getSeatDataReal());

			MovieSale movieSale=movieSaleService.get(order.getMovieSaleId()+"");
			Integer price = Integer.parseInt(movieSale.getCurrentPrice());
			order.setSiglePrice(price+ "");
			order.setTotalPrice(price * jsonSeat.size() + "");
			order.setSiglePrice(movieSale.getCurrentPrice());
			order.setTicketAmount(jsonSeat.size()+"");
			this.save(order);
			movieSale.setAlreadyJoin((Integer.parseInt(movieSale.getAlreadyJoin())+jsonSeat.size())+"");
			//上映信息里面的座位信息
			JSONArray jsonAjs = JSONArray.fromObject(movieSale.getAlreadyJoinSite());
			String ajs="[";
			for(int j = 0; j < jsonAjs.size(); j++){
				//页面选择的座位X Y轴详细信息
				JSONArray json = JSONArray.fromObject(jsonAjs.get(j));
				for (int i = 0; i < jsonSeat.size(); i++) {
					JSONObject json2 = JSONObject.fromObject(jsonSeat.get(i));
					if(Integer.parseInt(json2.getString("x"))==(j+1)){
                        //已选择的X轴座位信息
						int num=Integer.parseInt(json2.getString("y"))-1;
						String seat = json.get(num).toString();
						//修改座位信息
						if(seat.equals("n")){
							json.set(num,"y");
						}
					}
				}
				ajs += json.toString()+",";
			}
			movieSale.setAlreadyJoinSite(ajs+"]");
			movieSaleService.save(movieSale);
			}
		return order.getOrderNo();
	}

	/**
	 * 支付订单
	 * @param order
	 * @return
	 */
	@Transactional(readOnly = false)
	public String payOrder(Order order) {
		String result="1";
		order.setStatus("2");
		this.save(order);
		return result;
	}
}
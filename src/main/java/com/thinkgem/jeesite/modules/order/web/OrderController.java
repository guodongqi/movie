/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.order.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.Constants;
import com.thinkgem.jeesite.modules.sys.utils.JsonUtil;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.ibatis.ognl.Ognl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.movie.entity.Movie;
import com.thinkgem.jeesite.modules.moviesale.entity.MovieSale;
import com.thinkgem.jeesite.modules.order.entity.Order;
import com.thinkgem.jeesite.modules.order.service.OrderService;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单Controller
 * @author zhl
 * @version 2018-04-16
 */
@Controller
@RequestMapping(value = "${adminPath}/order/order")
public class OrderController extends BaseController {

	@Autowired
	private OrderService orderService;
	
	@ModelAttribute
	public Order get(@RequestParam(required=false) String id) {
		Order entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = orderService.get(id);
		}
		if (entity == null){
			entity = new Order();
		}
		return entity;
	}
	
	@RequiresPermissions("order:order:view")
	@RequestMapping(value = {"list", ""})
	public String list(Order order, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Order> page = orderService.findPage(new Page<Order>(request, response), order); 
		model.addAttribute("page", page);
		return "modules/order/orderList";
	}

	@RequiresPermissions("order:order:view")
	@RequestMapping(value = "form")
	public String form(Order order, Model model) {
		model.addAttribute("order", order);
		return "modules/order/orderForm";
	}

	@RequiresPermissions("order:order:edit")
	@RequestMapping(value = "save")
	public String save(Order order, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, order)){
			return form(order, model);
		}
		orderService.save(order);
		addMessage(redirectAttributes, "保存订单成功");
		return "redirect:"+Global.getAdminPath()+"/order/order/?repage";
	}
	
	@RequiresPermissions("order:order:edit")
	@RequestMapping(value = "delete")
	public String delete(Order order, RedirectAttributes redirectAttributes) {
		orderService.delete(order);
		addMessage(redirectAttributes, "删除订单成功");
		return "redirect:"+Global.getAdminPath()+"/order/order/?repage";
	}

	/**
	 * 生成订单
	 * @param order
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "createOrder")
	public String createOrder(Order order, Model model) {
		if(order==null){
			order = new Order();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		System.out.println(order.getOrderNo());
		List<Order> list = orderService.findList(order);
		
		order = list.get(0);
		model.addAttribute("order", order);
		model.addAttribute("start_time", sdf.format(order.getMovieSale().getStartTime()));
		model.addAttribute("site_info", JSONArray.fromObject(order.getSeatData()));
		model.addAttribute("site_info_real", JSONArray.fromObject(order.getSeatDataReal()));
		return "modules/front/make_sure_order";
	}
	
	private String htmlDecodeByRegExp (String str){
        String s = "";
        if(str.length() == 0) return "";
        s = str.replace("&amp;","&");
        s = s.replace("&lt;","<");
        s = s.replace("&gt;",">");
        s = s.replace("&nbsp;"," ");
        s = s.replace("&#39;","\'");
        s = s.replace("&quot;","\"");
        s = s.replace("\\n", "\n");
        s = s.replace("\\r", "\r");
        return s;
    }
	
	@RequestMapping("createOrderPost")
	@ResponseBody
	public JSONObject createOrderPost( @RequestParam(required = false) String seatData, @RequestParam(required = false) String seatDataReal,
			@RequestParam(required = false) String movieSaleId,
			  HttpServletRequest request,
			  HttpServletResponse response  ) {
		JSONObject jsonObject = new JSONObject();
		//如果传进来的用户Id为空就不用查了
		JSONObject data = new JSONObject();
		try {
			User user = UserUtils.getUser();
			if(StringUtils.isBlank(user.getId())){
				throw new RuntimeException( "没有登陆" );
			}else{
				Order order = new Order();
				String seatDataJson = htmlDecodeByRegExp(seatData);
				String seatDataRealJson = htmlDecodeByRegExp(seatDataReal);
				order.setSeatData(seatDataJson);
				order.setSeatDataReal(seatDataRealJson);
				order.setMovieSaleId(Long.parseLong(movieSaleId));
				String order_no=orderService.createOrder(order);
				data.put("order_no", order_no);
				JsonUtil.addRightData( jsonObject, "创建成功", data);
			}
		} catch ( JSONException je ) {
		je.printStackTrace();
		String message = je.getMessage();
		jsonObject = JsonUtil.addFialMessage( new JSONObject(), org.apache.commons.lang.StringUtils.isNotBlank( message ) ? message : "json转换异常" );
		} catch ( RuntimeException re ) {
			re.printStackTrace();
			String message = re.getMessage();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), org.apache.commons.lang.StringUtils.isNotBlank( message ) ? message : "未知的运行时异常" );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}
	

	/**
	 * 支付订单
	 * @param order
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "payOrder")
	public String payOrder(Order order, Model model) {
		String result=orderService.payOrder(order);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		System.out.println(order.getOrderNo());
		model.addAttribute("order", order);
		model.addAttribute("start_time", sdf.format(order.getMovieSale().getStartTime()));
		model.addAttribute("site_info", JSONArray.fromObject(order.getSeatData()));
		return "modules/front/order_success";
	}
	
	
	

	/**
	 * 查看全部订单
	 * @param order
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "orderList")
	public String orderList(Order order, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Order> page = orderService.findPage(new Page<Order>(request, response), order);
		model.addAttribute("page", page);
		return "modules/front/order_success";
	}
	
	
	
	/**
	 * 
	 *查询影片列表接口
	 *
	 * @author zhl
	 * @date 2017/11/28
	 */
	@RequestMapping("orList")
	@ResponseBody
	public JSONObject mvList( @RequestParam(required = false, defaultValue = "1") int pageNo,
							  @RequestParam(required = false, defaultValue = "6") int pageSize,
							  @RequestParam(required = false, defaultValue = "") String status,
							  HttpServletRequest request,
							  HttpServletResponse response) {
		JSONObject jsonObject = new JSONObject();

		
		System.out.println(pageNo);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		//如果传进来的用户Id为空就不用查了
		try {
			List<Map<String,Object>> data = Lists.newArrayList();
			Order order = new Order();
			Page page = new Page( request, response );
			page.setPageNo( pageNo );
			page.setPageSize( pageSize );
			
			User user = UserUtils.getUser();
			if(StringUtils.isBlank(user.getId())){
				throw new RuntimeException( "没有登陆" );
			}
			if(!"".equals(status)){
				System.out.println(status);
				order.setStatus(status);
			}
			order.setUser(user);
			order.setPage(page);
			List<Order> orList = orderService.findList(order);
			page.setList(orList);
			jsonObject.put( "count", page.getCount() );
			jsonObject.put( "pageNo", pageNo );
			jsonObject.put( "pageSize", pageSize );
			Long pageCount = page.getCount() % pageSize == 0? page.getCount()/pageSize : page.getCount()/pageSize+1;
			jsonObject.put("pageCount", pageCount);
			String[] statusList = {"等待支付", "已付款", "待使用", "待评价", "已完成" , "支付失败"};
			for(Order o:orList){
				Map<String, Object> etMap = Maps.newHashMap();
				if(o!=null) {
					
					etMap.put("create_time", sdf.format(o.getCreateTime()));
					etMap.put("start_time", sdf.format(o.getMovieSale().getStartTime()));
					etMap.put("orderId", o.getId());
					etMap.put("orderNo", o.getOrderNo());
					etMap.put("movieName", o.getMovie().getName());
					etMap.put("movieTime", o.getMovie().getTime());
					etMap.put("movieType", o.getMovie().getType());
					etMap.put("house", o.getMovieHouse().getName());
					etMap.put("movieBannerUrl", o.getMovie().getBannerUrl());
					etMap.put("price", o.getTotalPrice());
					etMap.put("status", statusList[Integer.parseInt(o.getStatus()) - 1]);
					etMap.put("site_info", JSONArray.fromObject(o.getSeatData()));
				}
				data.add( etMap );
			}
			if ( this.isPageOut( page.getCount(), pageNo, pageSize ))
				data = Lists.newArrayList();
			JSONArray arr = JSONArray.fromObject( data, JsonUtil.getJsonConfig( JsonUtil.SIPLE_DATA ) );
			JsonUtil.addRightData( jsonObject, "", arr );
		} catch ( Exception e ) {
			e.printStackTrace();
			jsonObject = JsonUtil.addFialMessage( new JSONObject(), Constants.ERROR_SERVER );
		}
		return jsonObject;
	}
	
	
	/**
	 * @param count    数据总条数
	 * @param pageNo   当前页码
	 * @param pageSize 每页条数
	 * @return
	 * @discription 判断当前页码是否超出最大页数
	 * @author pwj
	 * @date 2016/9/27 17:35
	 */
	public static boolean isPageOut( long count, int pageNo, int pageSize ) {
		if ( pageSize == 0 || count == 0 || pageNo == 0 )
			return true;
		long lastpage = count / pageSize;
		if ( count % pageSize != 0 )
			lastpage = lastpage + 1;
		if ( pageNo > lastpage )
			return true;
		else
			return false;
	}
	
	
}
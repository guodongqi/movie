package com.thinkgem.jeesite.modules.sys.utils;

/**
 * Created by admin on 2018/4/21.
 */
public class Constants {

    /**
     * 接口数据错误类型
     */
    public static final String	ERROR_NO_RESULT = "noresult";		//无结果
    public static final String	ERROR_SERVER = "servererror";		//服务器异常

    /**
     * 订单类型
     */
    /*public static final String	ERROR_NO_RESULT = "noresult";		//无结果
    public static final String	ERROR_SERVER = "servererror";		//服务器异常*/
    // 1等待支付、2已付款、3待使用、4待评价、5已完成、0支付失败
}

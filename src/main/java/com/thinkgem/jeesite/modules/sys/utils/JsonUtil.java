package com.thinkgem.jeesite.modules.sys.utils;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.utils.StringUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import net.sf.json.util.PropertyFilter;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class JsonUtil implements Serializable {
    //将此项应用到jsonconfig将排除page对象
    public static final String[] CONFIG_NO_PAGE = {"dbName", "isDel","currentUser","global","sqlMap","page","updateBy","updateDate","isNewRecord","delFlag"};
    //简化的json数据
    public static final String[] SIPLE_DATA = {"dbName", "isDel","currentUser","global","sqlMap","page","updateBy","updateDate","paperQuestionList","createBy","createDate","isNewRecord","delFlag"};
    //接口报错后返回信息
    public static final String SERVER_ERROR = "服务器异常，请稍后再试";
    //接口查询数据为空
    public static final String NO_RESULT = "无数据";
    //接口查询数据成功
    public static final String SUCCESS = "获取成功";

	public static List getDTOArray(JSONArray array, Class clazz){
        List list = new ArrayList();
        for(int i = 0; i < array.size(); i++){        
            JSONObject jsonObject = array.getJSONObject(i);        
             Object bean = JSONObject.toBean(jsonObject, clazz);
             list.add(bean);
        }   
        return list;
    }

    /**
     * json排除为空属性,防止自包含
     * @return
     */
    public static JsonConfig getJsonConfig(){
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        jsonConfig.setExcludes(new String[] {"dbName", "isDel","currentUser","global","sqlMap" });//排除不需要的属性
        jsonConfig.setJsonPropertyFilter(new PropertyFilter() {//排除为空的属性
            @Override
            public boolean apply(Object o, String s, Object o1) {
                return o1== null;
            }
        });
        return jsonConfig;
    }
    
    /**
     * json排除为空属性,防止自包含
     * @return
     */
    public static JsonConfig getJsonConfig(String strs[]){
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        jsonConfig.setExcludes(strs);//排除不需要的属性
        jsonConfig.setJsonPropertyFilter(new PropertyFilter() {//排除为空的属性
            @Override
            public boolean apply(Object o, String s, Object o1) {
                return o1== null;
            }
        });
        jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
        return jsonConfig;
    }

    /**
     * @discription 添加失败信息
     * @author pwj
     * @date 2016/7/20 15:16
     * @param jsonObject
     * @param msg 错误信息
     * @return
     */
    public static JSONObject addFialMessage(JSONObject jsonObject,String msg){
        if(jsonObject != null){
            jsonObject.put("result",0);
            if(Constants.ERROR_NO_RESULT.equals(msg)){
                jsonObject.put("msg",NO_RESULT);
            }else if(Constants.ERROR_SERVER.equals(msg)){
                jsonObject.put("msg",SERVER_ERROR);
            }else{
                jsonObject.put("msg",msg);
            }
        }
        return jsonObject;
    }
    
    /**
     * json排除为空和不需要的属性
     * @return
     */
    public static JsonConfig getJsonConfig(Class clazz){
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        Field[] fileds = clazz.getDeclaredFields();
        System.out.println(fileds.length);
        String[] strs = new String[fileds.length];
        for(int i=0;i<fileds.length;i++){
        	strs[i] = fileds[i].getName();
        }
        jsonConfig.setExcludes(strs);//排除不需要的属性
        jsonConfig.setJsonPropertyFilter(new PropertyFilter() {//排除为空的属性
            @Override
            public boolean apply(Object o, String s, Object o1) {
                return o1== null;
            }
        });
        return jsonConfig;
    }

    /**
     * @discription 接口请求成功返回数据
     * @author pwj
     * @date 2016/7/25 11:37
     * @param jsonObject 目标jsonobject
     * @param msg 成功信息
     * @param data 成功数据
     * @return 参数jsonObject
     */
    public static JSONObject addRightData(JSONObject jsonObject,String msg,Object data){
        if(jsonObject != null){
            jsonObject.put("result",1);
            if(StringUtils.isNotBlank(msg)){
                jsonObject.put("msg",msg);
            }else{
                jsonObject.put("msg",SUCCESS);
            }
            if(data != null){
                jsonObject.put("data",data);
            }else{
                jsonObject.put("data",new JSONObject());
            }
        }
        return jsonObject;
    }

    /**
     * @discription  接口请求成功返回数据（重载）
     * @author pwj
     * @date 2016/9/6 10:23
     * @param jsonObject
     * @param msg
     * @return
     */
    public static JSONObject addRightData( JSONObject jsonObject , String msg ){
        if(jsonObject != null){
            jsonObject.put( "result" , 1 );
            if(StringUtils.isNotBlank(msg)){
                jsonObject.put("msg",msg);
            }else{
                jsonObject.put("msg",SUCCESS);
            }
        }
        return jsonObject;
    }

    /**
     * @discription 获取对象的指定属性和值(暂时未定义对象中复杂对象的处理)
     * @author pwj
     * @date 2016/8/1 14:33
     * @param target 目标Object,也就是需要从此对象中得到指定属性
     * @param propNames 指定的属性名称
     * @return Map<属性名,属性值>
     */
    public static Map<String,Object> seriaProp(Object target,String[] propNames){
        Map<String,Object> data = Maps.newHashMap();
        if(target != null && propNames != null && propNames.length > 0){
            for(int i = 0 ; i < propNames.length ; i ++){
                String key = propNames[i];
                Class oc = target.getClass();
                for( ; oc != Object.class ; oc = oc.getSuperclass()){
                    try {
                        Field field = oc.getDeclaredField( key );
                        field.setAccessible( true );
                        Object value = field.get( target );
                        data.put( key,value );
                        break;
                    } catch (NoSuchFieldException e) {
                        //什么都不做
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return data;
    }

    /**
     * @discription 指定返回的字段必须为某些字段
     * @author pwj
     * @date 2016/8/1 17:04
     * @param includes 指定的字段
     * @return
     */
    public static JsonConfig propertyFilterJsonConfig(String[] includes){
        JsonConfig jsonConfig = new JsonConfig();

        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));

        if(includes != null){
            final StringBuffer includeStr = new StringBuffer();
            for(int i = 0 ; i < includes.length ; i ++){
                includeStr.append(includes[i] + ",");
            }
            jsonConfig.setJsonPropertyFilter(new PropertyFilter() {//排除为空的属性和不需要的属性
                @Override
                public boolean apply(Object o, String s, Object o1) {
                    String include = includeStr.toString();
                    return o1 == null || include.indexOf(s + ",") == -1;
                }
            });
        }

        return jsonConfig;
    }
}

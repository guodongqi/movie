<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>zhl管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/actor/actor/">zhl列表</a></li>
		<shiro:hasPermission name="actor:actor:edit"><li><a href="${ctx}/actor/actor/form">zhl添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="actor" action="${ctx}/actor/actor/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>影片ID：</label>
				<form:input path="movieId" htmlEscape="false" maxlength="20" class="input-medium"/>
			</li>
			<li><label>真实姓名：</label>
				<form:input path="realName" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>扮演角色名称：</label>
				<form:input path="actorName" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>演员照片：</label>
				<form:input path="icon" htmlEscape="false" maxlength="150" class="input-medium"/>
			</li>
			<li><label>0 女 、 1男：</label>
				<form:input path="sex" htmlEscape="false" maxlength="11" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<shiro:hasPermission name="actor:actor:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="actor">
			<tr>
				<shiro:hasPermission name="actor:actor:edit"><td>
    				<a href="${ctx}/actor/actor/form?id=${actor.id}">修改</a>
					<a href="${ctx}/actor/actor/delete?id=${actor.id}" onclick="return confirmx('确认要删除该zhl吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>
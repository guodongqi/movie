<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>鲨鱼影院</title>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
<link
	href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css"
	rel="stylesheet">
<style>
.mb-3 {
	margin-bottom: 0rem !important;
}

.jumbotron {
	padding: 1rem 2rem;
	background: -webkit-linear-gradient(#383156, #533C50, #6C4651, #383156);
	background: -moz-linear-gradient(#383156, #533C50, #6C4651, #383156);
	background: linear-gradient(#383156, #533C50, #6C4651, #383156);
	color: #FFFFFF;
	border-radius: 0;
}

.left-btn {
	position: absolute;
	top: 80px;
	left: -42.84px;
	font-size: 60px;
	color: #e5e5e5;
	cursor: pointer;
	padding-top: 5px;
}

.left-btn:hover {
	background-color: #6C4651;
	border-radius: 2px;
}

.right-btn {
	position: absolute;
	float: right;
	top: 80px;
	left: 100%;
	font-size: 60px;
	color: #e5e5e5;
	cursor: pointer;
	padding-top: 5px;
}

.right-btn:hover {
	background-color: #6C4651;
	border-radius: 2px;
}

.hidden {
	display: none
}

.list {
	max-width: 1200px;
	margin: auto auto;
	text-align: center
}

.movie-active {
	
}

.movie-list {
	width: 100%;
}

.m-item {
	padding-right: 10px;
	padding-left: 10px
}

.m-desc {
	padding-top: 20px;
	padding-bottom: 20px;
}

.m-title, .m-info {
	padding-left: 15px;
	padding-right: 15px;
}

.m-info {
	padding-bottom: 15px;
	font-size: 14px;
	font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB",
		"Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";
}

.m-info>span {
	color: #999999;
}

.m-info>label {
	padding-right: 20px;
	padding-left: 2px;
}

.m-title>span {
	padding-right: 5px;
}

.m-item-active {
	position: relative !important;
}

.m-item-active>img {
	width: 180px;
	height: auto;
	top: -5px;
	box-shadow: 5px 2px 6px #000
}

.m-item>img {
	border-radius: 3px;
}

.table tbody tr td {
	vertical-align: middle;
}

.buy-btn {
	color: #FFFFFF;
	background-color: #DE4C41;
	border-radius: 20px;
}

.over-time {
	font-size: 12px;
	color: #999999
}
</style>
</head>
<body>
	<!-- 导航栏 -->
	<div
		class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
		<h5 class="my-0 mr-md-auto font-weight-normal">
			<img src="${ctxStatic}/front/img/movie_icon.png" height="50px"
				width="auto"> 鲨鱼影院
		</h5>
		<nav class="my-2 my-md-0 mr-md-3">
			<a class="p-2 text-dark" href="${ctx }">首页</a> 
			 <a class="p-2 text-dark" href="${ctx}/api/movie/msRecommend">推荐</a>
			  <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
		<c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>
	</div>
	<!-- 导航栏结束 -->

	<!-- 首页大banner -->
	<main role="main">
	<div class="jumbotron">
		<div class="container" style="margin-left: 15px;">
			<div class="row">
				<div class="col">
					<img src="${ctxStatic}/front/img/movie_house.png"
						style="border-radius: 10px;">
				</div>
				<div class="col">
					<h3 class="display-4">鲨鱼影院</h3>
					<p>地址:北京市朝阳区朝阳路1000号</p>
					<p>观影热线:010-8223322</p>
				</div>
			</div>
		</div>
	</div>
	</main>
	<!-- 首页大banner结束 -->
	<div class="container">
		<p>鲨鱼电影</p>
		<p>鲨鱼电影（鲨鱼电影院）是由中国淘宝软件有限公司开发的一款生活类手机软件，提供丰富的正在热映和即将上映的电影资讯信息，包括预告片、高清海报与剧照、剧情介绍，以及网友评论等</p>
		<p>一直秉持“匠心”和“创新”的理念，为用户提供各类优质服务。目前，网易业务涵盖游戏、电商、新闻门户、邮箱、文化娱乐、在线教育、企业服务、工具应用等，是目前中国最大的互联网公司之一, 覆盖超过9亿的用户</p>
		<p>问：购买电影票后如何去影院看电影？</p>
		<p>答：1）在线选座影票观影办法：购买在线选座影票后，您会收到一条包含详细观影信息及兑票密码的短信，携带此短信去影院大厅对应的取票机上即可取票，网票网提供的影票前往网票网取票机取票，格瓦拉提供的影票前往格瓦拉取票机取票，也可寻求影院服务人员的帮助。</p>
		<p>&nbsp;&nbsp;2）电子兑换券观影方法：购买电子兑换券后，您会收到一条包含兑换码的短信，携带此短信去影院前台找售票人员即可兑票。电子兑换券有一定的有效期，需要您在有效期内到影院前台兑换。</p>
		<p>问：我购买的电影票现在不想看了，是否能退票？</p>
		<p>答：由于购票后，电子兑换数据已经生成，暂时不支持退票，希望您能理解。</p>
	</div>




</body>


<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script>
	
	
	
	
</script>
</html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <script type="text/javascript" src="${ctxStatic}/front/js/jquery.min.js"></script>
    <script src="${ctxStatic}/front/js/jquery-1.10.2.min.js"></script>
    <script src="${ctxStatic}/front/js/common.js"></script>
    <script src="${ctxStatic}/jquery/jquery.cookie.js" type="text/javascript"></script>
    <style>
        body{font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB", "Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";}
        .page{position: relative;width: 100%;}
        .icon-top{position: absolute;top: 10px;left:20%;}
        .icon-bottom{position: absolute;top: 120px;left:20%}
        .text-top{font-size: 25px;color: #1F508A}
        .login-div{position: absolute;width: 25%;left: 65%;top: 100px;}
        .login-form{border: 1px solid #f5f5f5;padding: 30px;border-radius: 5px;background-color: #FFFFFF; box-shadow:5px 2px 6px #999}
        .login-btn{width:100%;background-color: #1F508A;border:1px solid #1F508A;}
        .login-form > .form-group,.form-check{padding-top: 20px;}
    </style>
</head>
<body>

<div class="page">
    <div class="icon-top">
        <img src="${ctxStatic}/front/img/movie_icon.png" width="100px" height="auto"><span class="text-top">鲨鱼影院</span>
    </div>

    <div class="icon-bottom">
        <img src="${ctxStatic}/front/img/登录页面.png">
    </div>

    <div class="login-div">
        <div class="login-form">
            <form id="loginForm" class="form-signin" action="${ctx}/login" method="post">
                <div class="form-group">
                    <label for="username">手机号</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp"
                           placeholder="输入手机号">
                </div>
                <div class="form-group">
                    <label for="password">密码</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="密码">
                </div>
                <div class="form-check">
                    <input type="checkbox" id="rememberMe" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">记住密码</label>
                </div><br/>
                <p id="message"></p>
                <input class="btn btn-primary login-btn" type="button" onclick="submitUser();" value="登录"/>
                
            </form>
            <br/>
        	<span onclick="registerUser();" style="color: #DE4C41;padding-top:10px;cursor:pointer">没有账户？去注册</span>
        </div>
        
    </div>
</div>


</body>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="dist/js/showtip.js"></script>
<script src="dist/js/jquery.min.js"></script>
<script>

    $(document).ready(function() {
        //登录前删除左侧菜单sessionStorage
        var username = cookie("username");
        var password = cookie("password");
        if(username != null&&username != "null"){
            $("#username").val(username);
            document.getElementById("rememberMe").checked = true;
        }
        if(password != null&&password != "null"){
            $("#password").val(password);
        }

        var message = "${message}";
        if(message != ""){
            $("#message").html(message);
        }
    });

    document.onkeydown = function(e) {
        e = e || window.event;
        if(e.keyCode == 13) {
            submitUser();
        }
    }

    function registerUser(){
        window.location.href="${ctx}/api/movie/registerUser"
    }

    function submitUser(){
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        var validateCode = $("#validateCode").val();
        //$.cookie('rememberMeStr',!!$('#rememberMe').attr('checked') ? '0':null);
        if(username == ""){
            $("#message").html("手机号不能为空！");
            return false;
        }
        else if(password == ""){
            $("#message").html("密码不能为空！");
            return false;
        }
        else{
            $("#loginForm").attr("action","${ctx}/login");
            $("#loginForm").submit();
        }
    }
</script>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>确认订单</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="${ctxStatic}/front/css/iF.step.css" rel="stylesheet">
    <style>
        body{font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB", "Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";}
        .sand-clock-block{width:100%;min-height: 100px;background-color: #FDF4F3;}
        .text-1 {position: relative;color: #999999;padding-left: 20px;}
        .text-2 {color:#DE4C41 ;padding-left: 20px;}
        .min, .second{font-size: 30px;color: #DE4C41}
        .thead-light{background-color: #F7F7F7}
        .buy-btn{color: #FFFFFF;background-color: #DE4C41;border-radius: 20px;}
    </style>
</head>
<!-- 导航栏 -->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">
        <img src="${ctxStatic}/front/img/movie_icon.png" height="50px" width="auto">
        鲨鱼影院
    </h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="${ctx}">首页</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/msRecommend">推荐</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
    <c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>
</div>
<!-- 导航栏结束 -->

<!-- 流程图 -->
<div class="container">
    <ol class="ui-step ui-step-red ui-step-4">
        <li class="step-start step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">1</span>
                <span class="ui-step-cont-text">选择影片场次</span>
            </div>
        </li>
        <li class="step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">2</span>
                <span class="ui-step-cont-text">选择座位</span>
            </div>
        </li>
        <li class="step-active">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">3</span>
                <span class="ui-step-cont-text">10分钟内付款</span>
            </div>
        </li>
        <li class="step-end">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">4</span>
                <span class="ui-step-cont-text">取票到店观影</span>
            </div>
        </li>
    </ol>
</div>
<!-- 流程导图 -->
<br/>
<!-- 倒计时 -->
<div class="sand-clock-block container">
    <div class="row" style="float: left">
        <div class="sand-clock-icon">
            <i class="fa fa-hourglass-half" aria-hidden="true"
               style="color: #F8D4D3;font-size: 60px;line-height: 100px;padding-left: 30px;"></i>
        </div>
        <div class="sand-clock-text">
            <div class="text-1"><span>请在<span class="min">&nbsp;10&nbsp;</span>分钟<span class="second"></span>之内付款</span></div>
            <div class="text-2"><span>超时订单会自动取消，如有疑问，请拨打客服热线 000000000</div>
        </div>
    </div>
</div>
<!-- 倒计时结束 -->

<!-- 订单信息 -->
<br/><br/>
<div class="container"><div class="row">
    <table class="table" style="text-align: center;border: 1px solid #f7f7f7">
        <thead class="thead-light">
        <tr>
            <th scope="col">影片</th>
            <th scope="col">放映时间</th>
            <th scope="col">语言版本</th>
            <th scope="col">售价</th>
            <th scope="col">座位信息</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>${order.movie.name}</td>
            <th scope="row">${start_time}</th>
            <td>${order.movieSale.langueVersion }</td>
            <td><span style="color: #DE4C41">¥ ${order.totalPrice} </span></td>
            <td>
                <span class="m-house-num">${order.movieHouse.name}</span>&nbsp;&nbsp;&nbsp;<span id="m-site">
                	<c:forEach items="${site_info}" varStatus="i" var="item" >  
			           ${item.x}排${item.y}座 
        			</c:forEach>  
                	
                	
                	
                </span>
            </td>
        </tr>
        </tbody>
    </table>

</div>
</div>
<!-- 订单信息 -->

<!-- 支付按钮 -->
<div class="pay-btn container">
    <div class="row" style="width: 100%;position: relative">
        <div class="col-sm-11"></div>
        <div class="col-sm-1"><button class="btn buy-btn"  style="float: right !important;width: 100px;">去支付</button></div>
    </div>
</div>
<!-- 支付按钮结束 -->

</body>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script>
	var order_id = ${order.id}
    $(document).on("click", ".buy-btn", function () {
    	window.location.href="${ctx}/order/order/payOrder?id="+order_id
    })
</script>
</html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>鲨鱼影院</title>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
<link
	href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css"
	rel="stylesheet">
<style>
.mb-3 {
	margin-bottom: 0rem !important;
}

.jumbotron {
	padding: 1rem 2rem;
	background: -webkit-linear-gradient(#383156, #533C50, #6C4651, #383156);
	background: -moz-linear-gradient(#383156, #533C50, #6C4651, #383156);
	background: linear-gradient(#383156, #533C50, #6C4651, #383156);
	color: #FFFFFF;
	border-radius: 0;
}

.left-btn {
	position: absolute;
	top: 80px;
	left: -42.84px;
	font-size: 60px;
	color: #e5e5e5;
	cursor: pointer;
	padding-top: 5px;
}

.left-btn:hover {
	background-color: #6C4651;
	border-radius: 2px;
}

.right-btn {
	position: absolute;
	float: right;
	top: 80px;
	left: 100%;
	font-size: 60px;
	color: #e5e5e5;
	cursor: pointer;
	padding-top: 5px;
}

.right-btn:hover {
	background-color: #6C4651;
	border-radius: 2px;
}

.hidden {
	display: none
}

.list {
	max-width: 1200px;
	margin: auto auto;
	text-align: center
}

.movie-active {
	
}

.movie-list {
	width: 100%;
}

.m-item {
	padding-right: 10px;
	padding-left: 10px
}

.m-desc {
	padding-top: 20px;
	padding-bottom: 20px;
}

.m-title, .m-info {
	padding-left: 15px;
	padding-right: 15px;
}

.m-info {
	padding-bottom: 15px;
	font-size: 14px;
	font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB",
		"Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";
}

.m-info>span {
	color: #999999;
}

.m-info>label {
	padding-right: 20px;
	padding-left: 2px;
}

.m-title>span {
	padding-right: 5px;
}

.m-item-active {
	position: relative !important;
}

.m-item-active>img {
	width: 180px;
	height: auto;
	top: -5px;
	box-shadow: 5px 2px 6px #000
}

.m-item>img {
	border-radius: 3px;
}

.table tbody tr td {
	vertical-align: middle;
}

.buy-btn {
	color: #FFFFFF;
	background-color: #DE4C41;
	border-radius: 20px;
}

.over-time {
	font-size: 12px;
	color: #999999
}
</style>
</head>
<body>
	<!-- 导航栏 -->
	<div
		class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
		<h5 class="my-0 mr-md-auto font-weight-normal">
			<img src="${ctxStatic}/front/img/movie_icon.png" height="50px"
				width="auto"> 鲨鱼影院
		</h5>
		<nav class="my-2 my-md-0 mr-md-3">
			<a class="p-2 text-dark" href="${ctx }">首页</a> 
			 <a class="p-2 text-dark" href="${ctx}/api/movie/msRecommend">推荐</a>
			  <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
		<c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>

	</div>
	<!-- 导航栏结束 -->

	<!-- 首页大banner -->
	<main role="main">
	<div class="jumbotron">
		<div class="container" style="margin-left: 15px;">
			<div class="row">
				<div class="col">
					<img src="${ctxStatic}/front/img/movie_house.png"
						style="border-radius: 10px;">
				</div>
				<div class="col">
					<h3 class="display-4">鲨鱼影院</h3>
					<p>地址:北京市朝阳区朝阳路1000号</p>
					<p>观影热线:010-8223322</p>
					<p>
					</p>
				</div>
			</div>
		</div>
	</div>
	</main>
	<!-- 首页大banner结束 -->

	<!-- 上映的影片 -->
	<div class="list" style="position: relative">
		<i class="fa fa-chevron-left left-btn" aria-hidden="true"></i>
		<div class="movie-list movie-active" style="width: 100%"
			id="movie-list"></div>

		<i class="fa fa-chevron-right right-btn" aria-hidden="true"></i>
	</div>
	<div class="container m-desc">

		<div id="movie-desc">
			<div class="row m-title">
				<span><h3>爆裂无声</h3></span> <span style="color: #EFB13E"><h3>8.0</h3></span>
				<span style="padding-top: 10px;"><h9>分</h9></span>
			</div>
			<div class="row m-info">
				<span>时长 :</span><label> 120分钟 </label><span>类型 :</span> <label>惊悚</label>
				<span>主演 :</span><label> 宋洋,姜武,袁文康</label>
			</div>
		</div>
	</div>
	<!-- 上映的影片结束 -->

	<!-- 场次列表 -->
	<div class="container" id="movie-sale-list">
		

	</div>





</body>


<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script>
	var page = 1;
	moive_list();
	

	// 点击后一个 影片横向轮播列表  的按钮
	$(document).on("click", ".right-btn", function() {
		if (page == 2) {
			showtip("最后一页啦～");
			return false;
		}
		page = 2;
		moive_list()
	});

	// 点击前一个 影片横向轮播列表 的按钮
	$(document).on("click", ".left-btn", function() {
		if (page == 1) {
			showtip("已经是第一页啦～");
			return false;
		}
		page = 1;
		moive_list()
	});

	// 点击m-item
	$(document).on("click", ".m-item", function() {
		var _this = $(this);
		_this.parent().children().each(function() {
			$(this).removeClass("m-item-active");
		});
		_this.addClass("m-item-active");
		// 改变介绍的展示
		var movie_id = _this.attr("movie_id");
		$(".movie-desc").addClass("hidden");
		$("#movie-desc-" + movie_id).removeClass("hidden");
		
		// 显示下面的上映时间列表
		moive_sale_list(movie_id);

	});


	// 请求所有的电影列表
	function moive_list() {
		$.getJSON(
						"${ctx}/api/movie/mvList",
						{
							"pageNo" : page
						},
						function(data) {
							var info = data.data;
							var html = "";
							var movie_desc = "";
							for (var i = 0; i < info.length; i++) {
								var active = "";
								if (i == 0) {
									active = "m-item-active"
									moive_sale_list(info[i].movieId);
								}
								html += '<span class="m-item '+active+'" movie_id="'+info[i].movieId+'"> '
										+ '<img src="'+info[i].movieBannerUrl +'">'
										+ '</span>'
							}
							;
							console.log(info)
							for (var i = 0; i < info.length; i++) {
								var hidden = "hidden";
								if (i == 0) {
									hidden = ""
								}
								movie_desc += '<div class= "movie-desc ' + hidden + '" id="movie-desc-' + info[i].movieId + '"><div class="row m-title">'
										+ '<span><h3>'
										+ info[i].movieName
										+ '</h3></span>'
										+ '<span style="color: #EFB13E"><h3>'
										+ info[i].movieLevel
										+ '</h3></span>'
										+ '<span style="padding-top: 10px;"><h9>分</h9></span>'
										+ '</div>'
										+ '<div class="row m-info" >'
										+ '<span>时长 :</span><label> '
										+ info[i].movieTime
										+ '  </label><span>类型 :</span> <label>'
										+ info[i].movieType
										+ '</label> <span>主演 :</span><label> '
										+ info[i].movieActors
										+ '</label>'
										+ '</div></div>'
							}
							;
							$("#movie-list").html(html);
							$("#movie-desc").html(movie_desc);
						})
	}
	// 上一页
	function moive_sale_list(mvId) {
		$.getJSON("${ctx}/api/movie/msList", {"mvId" : mvId}, function(data) {
			console.log(data);
			var info = data.data;
			var html = '<table class="table table-striped" style="text-align: center">'+		
					   '<thead>'+
							'<tr>'+
								'<th scope="col">放映时间</th>'+
								'<th scope="col">语言版本</th>'+
								'<th scope="col">放映厅</th>'+
								'<th scope="col">售价</th>'+
								'<th scope="col">选座购票</th>'+
							'</tr>'+
						'</thead>'+
						'<tbody>'
			for(var i=0; i<info.length; i++){
				html += '<tr>'+
					'<th scope="row">'+info[i].startTime+'<br>'+
					'<span class="over-time">'+info[i].endTime+'散场</span></th>'+
					'<td>原版2D</td>'+
					'<td>'+info[i].movieHouse+'</td>'+
					'<td><span style="color: #DE4C41">¥'+info[i].currentPrice+'</span></td>'+
					'<td>'+
						'<button class="btn btn-sm buy-btn" movie_sale_id="'+info[i].msId+'">选座购票</button>'+
					'</td>'+
				'</tr>'
			
			}
			html +='</tbody>'+
			'</table>';
			$("#movie-sale-list").html(html)
			
			
		})
	}
	
	// 点击选座购票
	
	$(document).on("click",".buy-btn", function(){
		ms_id = $(this).attr("movie_sale_id")
		window.location.href="${ctx}/api/movie/msDetail?id="+ms_id
		
	})
		
		
	
	
	
</script>
</html>
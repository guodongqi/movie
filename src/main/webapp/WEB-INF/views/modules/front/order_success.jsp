<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>订单完成</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="${ctxStatic}/front/css/iF.step.css" rel="stylesheet">
    <style>
        body{font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB", "Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";}
        .sand-clock-block{width:100%;min-height: 100px;background-color: #FDF4F3;}
        .text-1 {position: relative;color: #999999;padding-left: 20px;padding-top: 10px;}
        .text-2 {color:#DE4C41 ;padding-left: 20px;}
        .thead-light{background-color: #F7F7F7}
        .success{color: #99D65D;font-size: 25px;}
        .btn-success{color: #DE4C41;background-color: #ffffff;border-color: #DE4C41;border-radius: 0px;}
        .btn-success:hover{color: #ffffff;background-color: #DE4C41;border-color: #DE4C41;}
        .btn-success-reply{color: #ffffff;background-color: #DE4C41;border-color: #DE4C41;border-radius: 0px;}
        .btn-success-reply:hover{color: #DE4C41;background-color: #ffffff;border-color: #DE4C41;}
    </style>
</head>
<body>
<!-- 导航栏 -->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">
        <img src="${ctxStatic}/front/img/movie_icon.png" height="50px" width="auto">
        鲨鱼影院
    </h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="${ctx}/">首页</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/recommend_movie">推荐</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
    <c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>
</div>
<!-- 导航栏结束 -->

<!-- 流程图 -->
<div class="container">
    <ol class="ui-step ui-step-red ui-step-4">
        <li class="step-start step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">1</span>
                <span class="ui-step-cont-text">选择影片场次</span>
            </div>
        </li>
        <li class="step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">2</span>
                <span class="ui-step-cont-text">选择座位</span>
            </div>
        </li>
        <li class="step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">3</span>
                <span class="ui-step-cont-text">10分钟内付款</span>
            </div>
        </li>
        <li class="step-end step-active">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">4</span>
                <span class="ui-step-cont-text">取票到店观影</span>
            </div>
        </li>
    </ol>
</div>
<!-- 流程导图 -->

<!-- 倒计时 -->
<div class="sand-clock-block container">
    <div class="row" style="float: left">
        <div class="sand-clock-icon">
            <i class="fa fa-film" aria-hidden="true"
               style="color: #F8D4D3;font-size: 60px;line-height: 100px;padding-left: 30px;"></i>
        </div>
        <div class="sand-clock-text">
            <div class="text-1"><span><span class="success">支付成功！</span>到店提供订单号或者手机号码取票观影。</span></div>
            <div class="text-2"><span>如有疑问，请拨打客服热线 000000000</span></div>
        </div>
    </div>
</div>
<!-- 倒计时结束 -->

<!-- 订单信息 -->
<br/><br/>
<div class="container"><div class="row">
    <table class="table" style="text-align: center;border: 1px solid #f7f7f7">
        <thead class="thead-light">
        <tr>
            <th scope="col">订单号</th>
            <th scope="col">影片</th>
            <th scope="col">放映时间</th>
            <th scope="col">语言版本</th>
            <th scope="col">售价</th>
            <th scope="col">选座购票</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>${order.orderNo }</td>
            <td>${order.movie.name }</td>
            <th scope="row">${start_time }</th>
            <td>原版2D</td>
            <td><span style="color: #DE4C41">¥${order.totalPrice }</span></td>
            <td>
                <span class="m-house-num">3</span>号厅&nbsp;&nbsp;&nbsp;<span id="m-site">
                <c:forEach items="${site_info}" varStatus="i" var="item" >  
			           ${item.x}排${item.y}座 
        			</c:forEach>  
                </span>
            </td>
        </tr>
        </tbody>
    </table>

</div>
</div>
<!-- 订单信息 -->


<!-- 按钮组 -->
<div class="container">
    <div class="" style="text-align: center">
        <span class="btn btn-success">返回首页</span>&nbsp;
        <span class="btn btn-success-reply">前往个人中心</span>
    </div>

</div>
<!-- 按钮组结束 -->

</body>


<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script>
    $(document).on("click", ".btn-success", function () {
        window.location.href='${ctx}';
    });
    $(document).on("click", ".btn-success-reply", function () {
        window.location.href='${ctx}/sys/user/usercenter'
    })
</script>
</html>
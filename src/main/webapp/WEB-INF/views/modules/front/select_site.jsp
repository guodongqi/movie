<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>选座购票</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="${ctxStatic}/front/css/iF.step.css" rel="stylesheet">
    <style>
        body{font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB", "Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";}
        .site-item,.site-item-no{min-height: 25px;min-width: 26px;width: 25px;height: 26px;margin: 2px;}
        .site-item-no{color: #999999;font-size: 18px;text-align: center;padding-right: 10px;}
        .site-info{padding-left: 10px;padding-top: 20px;text-align: center;width: 100%}
        .site-row{margin-left: auto;margin-right: auto;width: 100%}
        .site-screen{text-align: center}
        .site-screen>span{color: #999999; font-size: 15px;padding-right: 10px;}
        .pingmu{border: 2px solid #DE4C41;height: 20px;margin-left: 20%;margin-right: 20%;margin-top: 10px;border-top-right-radius: 20px;border-top-left-radius: 20px;background-color: #DE4C41}

        .body-row{border: 2px solid #DE4C41; margin: 20px;min-height: 500px;border-radius: 10px;height: auto}
        .movie-info>img{border: 4px solid #ffffff;box-shadow:5px 5px 3px #e0e0e0}
        .movie-desc{position: absolute;top: 0px;left:150px;}
        .movie-desc>#movie-type{padding-top: 30px;}
        .key{color: #999999}
        .movie-screen{margin-top: 20px;margin-bottom:10px;border-bottom: 1px dashed #999999;}
        .site-selected>.site-selected-info, .site-price{color: #999999}
        #price{color: #DE4C41}
        .site-selected{border-bottom: 1px dashed #999999;  }
        .site-price{padding-top: 50px;}
        .buy{padding-top: 30px;padding-bottom: 20px;}
        .buy-btn{color: #FFFFFF;background-color: #DE4C41;border-radius: 20px;width: 100%}
        #site-number{margin-right: 1px;}
        .site-tag{border: 1px solid #DE4C41; border-radius: 3px;padding-right: 1px;background-color:#DE4C41;color: #ffffff }
    </style>
</head>
<body>
<!-- 导航栏 -->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">
        <img src="${ctxStatic}/front/img/movie_icon.png" height="50px" width="auto">
        鲨鱼影院
    </h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="${ctx }/">首页</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/recommend_movie">推荐</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
    <c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>
</div>
<!-- 导航栏结束 -->
<!-- 流程图 -->
<div class="container">
    <ol class="ui-step ui-step-red ui-step-4">
        <li class="step-start step-done">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">1</span>
                <span class="ui-step-cont-text">选择影片场次</span>
            </div>
        </li>
        <li class="step-active">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">2</span>
                <span class="ui-step-cont-text">选择座位</span>
            </div>
        </li>
        <li class="step-normal">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">3</span>
                <span class="ui-step-cont-text">10分钟内付款</span>
            </div>
        </li>
        <li class="step-end">
            <div class="ui-step-line"></div>
            <div class="ui-step-cont">
                <span class="ui-step-cont-number">4</span>
                <span class="ui-step-cont-text">取票到店观影</span>
            </div>
        </li>
    </ol>
</div>
<!-- 流程图结束 -->

<!-- 选择座位 -->
<div class="container" style="padding-top: 20px;">
    <div class="row body-row">
        <div class="col-sm-8">
            <div class="site-screen">
                <div class="pingmu"></div>
                <div>屏幕</div>
                <span><img src="${ctxStatic}/front/img/site_white.png">&nbsp;&nbsp;可选座位</span>
                <span><img src="${ctxStatic}/front/img/site_disabled.png">&nbsp;&nbsp;不可选座位</span>
                <span><img src="${ctxStatic}/front/img/site_checked.png">&nbsp;&nbsp;已选座位</span>
            </div>
            <div class="site-info" id="site-info">
				
            </div> 
        </div>
        <div class="col-sm-4" style="background-color: #F9F9F9" id="movie-info-div">
            
        </div>
        <input type="hidden" id="movie-sale-id" movie_id="${movieSale.id }"/>
    </div>
</div>
<!-- 选择座位结束 -->


</body>

<%-- <script id="site-info-script" type="text/html">
    

</script> --%>

<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script src="${ctxStatic}/front/js/baiduTemplate.js"></script>

<script>
    $(function(){
    	var select_site_arr = [];
    	var select_site_arr_real = [];
    	var movie_sale_id = $("#movie-sale-id").attr("movie_id");
    	console.log(movie_sale_id);
    	site_info(movie_sale_id);

        // 选择座位以及反选
        $(document).on("click", ".white", function () {
            if(is_five()){showtip("最多只能选择5张票");return false;}
            $(this).html("<img src='${ctxStatic}/front/img/site_checked.png'>");
            $(this).removeClass("white");
            $(this).addClass("checked");
            selected_site_num_info()
        });
        $(document).on("click", ".checked", function () {
            $(this).html("<img src='${ctxStatic}/front/img/site_white.png'>");
            $(this).removeClass("checked");
            $(this).addClass("white");
            selected_site_num_info()
        });

        //判断是否只选择了5张票
        function is_five() {
            var i = 0;
            $(".checked").each(function () {
                i = i + 1;
            });
            return i >= 5;
        };

        //返回选中的座位信息
        function selected_site_num_info() {
            var site_str_html = "";
            var i = 0;
            var arr = [];
            var arr_real = [];
            $(".checked").each(function () {
                var row_num = $(this).attr("row_num");
                var seat_num = $(this).attr("seat_num");
                var line_num = $(this).attr("line_num");
                arr.push({"x": row_num, "y": line_num})
                arr_real.push({"x":row_num, "y": seat_num})
                site_str_html += "<span class='site-tag'>"+row_num + "排" + seat_num +"</span>&nbsp;" ;
                i = i + 1;
            });
            $("#price").html(i * parseFloat($("#movie-single-price").html()));
            $("#site-number").html(site_str_html);
            select_site_arr = arr;
            select_site_arr_real = arr_real;
            console.log(select_site_arr);
            console.log(select_site_arr_real);
        }

        // 点击付款按钮
        $(document).on("click", ".buy-btn", function () {
        	var seatDate = JSON.stringify(select_site_arr);
        	
        	var seatDateReal = JSON.stringify(select_site_arr_real);
        	//window.location.href="${ctx}/order/order/createOrder?seatData="+seatDate+"&movieSaleId="+movie_sale_id;
        
			$.getJSON("${ctx}/order/order/createOrderPost", {"seatData": seatDateReal, "movieSaleId": movie_sale_id, "seatDataReal": seatDate}, function(data){
				if(data.result == 1){
					console.log(data);
					window.location.href="${ctx}/order/order/createOrder?orderNo="+data.data.order_no
				}
				
				
			}) 
            		
            		
        })
        
        
        //获取座位信息
        function site_info(movie_sale_id){
        	$.getJSON("${ctx}/api/movie/msSiteInfo",{"msId": movie_sale_id}, function(data){
        		var data = data.data;
        		console.log(data);
        		var info = data.alreadyJoinSite
        		var html = ""
        		var movie_info_html = '<div class="movie-info" style="position: relative;margin-top: 20px;">'+
						                '<img src="'+data.bannerUrl+'" width="130" height="auto">'+
						                '<div class="movie-desc">'+
						                    '<div id="movie-name"><h3>'+data.movieName+'</h3></div>'+
						                    '<div id="movie-type">类型：'+data.movieType+'</div>'+
						                    '<div id="movie-time">时长：'+data.time+'分钟</div>'+
						                '</div>'+
							            '</div>'+
							            '<div class="movie-screen">'+
							                '<div class="">'+
							                    '<span class="key">影院：</span><span class="value">鲨鱼影院</span>'+
							                '</div>'+
							                '<div class="">'+
							                    '<span class="key">影厅：</span><span class="value">'+data.house+'</span>'+
							                '</div>'+
							                '<div class="">'+
							                    '<span class="key">版本：</span><span class="value">英语3D</span>'+
							                '</div>'+
							                '<div class="">'+
							                    '<span class="key">场次：</span><span class="value">'+data.startTime+'</span>'+
							                '</div>'+
							                '<div class="">'+
							                    '<span class="key">票价：</span>¥&nbsp;<span class="value" id="movie-single-price">'+data.price+'</span>/张'+
							                '</div>'+
							            '</div>'+
							            '<div class="site-selected">'+
							                '<div class="site-selected-info">'+
							                    '座位：<span id="site-number">一次最多选择五个</span>'+
							                '</div>'+
							                '<div class="site-price">'+
							                    '价格：<span id="price">¥0</span>'+
							                '</div>'+
							            '</div>'+
							            '<div class="buy">'+
							                '<button class="btn buy-btn">确认并付款</button>'+
							            '</div>'

        		for(var i=0; i < info.length; i++){ 
        			var row=info[i];
        			html += '<div class="site-row row">'+
				        '<span class="site-item-no">'+(i+1)+'</span>'
				    var seat_num = 0;
        			for(var j=0; j < row.length; j++){
        				
        				var row_i = row[j];
        				if(row_i == 'x'){
				        	html += '<span class="site-item"></span>'
				        }else if(row_i == 'y'){
				        	seat_num = seat_num + 1;
				        	html += '<span class="site-item disabled"><img src="${ctxStatic}/front/img/site_disabled.png"></span>'
				        }else if(row_i == 'n'){
				        	seat_num = seat_num + 1;
				        	html += '<span class="site-item white" row_num="'+ (i+1) +'" seat_num="'+seat_num+'" line_num="'+ (j+1) +'"><img src="${ctxStatic}/front/img/site_white.png"></span>'
				        }
        			}
        			html += '</div>';
        		}
        		
        		
        		$("#site-info").html(html);
        		$("#movie-info-div").html(movie_info_html);
        	})
        }


    })


</script>
</html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>个人中心</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${ctxStatic}/front/css/movie.css">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href="${ctxStatic}/front/css/iF.step.css" rel="stylesheet">
    <style>
        body{font-family: Helvetica, Tahoma, Arial, "PingFang SC", "Hiragino Sans GB", "Heiti SC", STXihei, "Microsoft YaHei", SimHei, "WenQuanYi Micro Hei";}
        .sand-clock-block{width:100%;min-height: 100px;background-color: #FDF4F3;}
        .text-1 {position: relative;color: #999999;padding-left: 20px;}
        .text-2 {color:#DE4C41 ;padding-left: 20px;}
        .min, .second{font-size: 30px;color: #DE4C41}
        .thead-light{background-color: #F7F7F7}
        .buy-btn{color: #FFFFFF;background-color: #DE4C41;border-radius: 20px;}
    </style>
</head>
<body>

<!-- 导航栏 -->
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">
        <img src="${ctxStatic}/front/img/movie_icon.png" height="50px" width="auto">
        鲨鱼影院
    </h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="${ctx}/">首页</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/recommend_movie">推荐</a>
        <a class="p-2 text-dark" href="${ctx}/api/movie/aboutUs">关于我们</a>
    <c:choose>
			<c:when test="${not empty fns:getUser().id}">
				<a class="p-2 text-dark" href="${ctx}/sys/user/usercenter">个人中心</a>&nbsp;&nbsp;
				<a class="p-2 text-dark" href="${ctx}/logout">退出</a>
			</c:when>
			
			<c:otherwise>
				<a class="p-2 text-dark" href="${ctx}/frontLogin">登录</a>

			</c:otherwise>

		</c:choose>
		</nav>
</div>
<!-- 导航栏结束 -->


<!-- 竖向导航栏 -->
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action " tag="user_info">个人信息</a>
                <a href="#" class="list-group-item list-group-item-action active" tag="order_all_list">全部订单</a>
                <a href="#" class="list-group-item list-group-item-action" tag="order_wait_list">待付款订单</a>
            </div>
        </div>
        <div class="col-sm-9" style="" id="right-block">

        </div>
    </div>
</div>
<!-- 竖向导航栏结束 -->


</body>

<script id="order-list-script" type="text/html">
    <%-- <% for (var i=0 ; i< data.length ; i++){ %> <% var row = data[i]%>

    



    <% } %> --%>
</script>

<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="${ctxStatic}/front/js/showtip.js"></script>
<script src="${ctxStatic}/front/js/jquery.min.js"></script>
<script src="${ctxStatic}/front/js/baiduTemplate.js"></script>
<script>
	var page = 1;
	var status = ""
	order_list_func(page, status);
	
    $(document).on("click", ".buy-btn", function () {
    	var order_id = $(this).attr("order_id")
    	window.location.href="${ctx}/order/order/payOrder?id="+order_id
    });
    
    $(document).on("click", ".list-group-item-action", function () {
        $(".list-group-item-action").removeClass("active");
        $(this).addClass("active");
        if($(this).attr("tag") == "order_all_list"){
        	page = 1
        	status = ""
            order_list_func(page, status);
        }else if($(this).attr("tag") == "user_info"){
        	user_info()
        }else if($(this).attr("tag") == "order_wait_list"){
        	page = 1
        	status = "1"
        	order_list_func(page, status);
        }
    });
    
    $(document).on("click", ".page-link", function(){
    	page = $(this).html();
    	console.log($(this).html())
    	order_list_func(page, status);
    })
    
    function user_info(){
    	var html = '<div style="border:1px solid #e5e5e5;min-height:200px;border-radius:4px;padding:10px;">姓名:${user.name}<br/>手机号:${user.loginName}</div>';
    	$("#right-block").html(html);
    	
    }
    

    function order_list_func(page, status) {
    	var html = "";
        $.getJSON("${ctx}/order/order/orList", {"pageNo": page, "pageSize": 6, "status": status}, function(data){
        	console.log(data);
        	var info = data.data;
        	for (var i=0; i< info.length; i++){
        	html += '<div style="border:1px solid #e5e5e5;">'+
			        '<div style="background-color: #F7F7F7;color: #9b9b9b;height: 50px;line-height: 50px;padding-left: 10px;">'+
			            '<span style="color: #222222">'+info[i].create_time+'</span> &nbsp; 订单号:'+info[i].orderNo +
			        '</div>'+
			        '<div class="row" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">'+
			            '<div class="col-sm-2">'+
			                '<img src="'+info[i].movieBannerUrl+'" width="100px" height="auto">'+
			            '</div>'+
			            '<div class="col-sm-3" style="line-height: 30px;">'+
			                '<div>'+info[i].movieName+'</div>'+
			                '<div>鲨鱼影院</div>'+
			                '<div>'+
			                    '<span>'+info[i].house+'</span>&nbsp;'+
			                '</div>'+
			                '<div>'+
			                    info[i].start_time+
			                '</div>'
			                var row = info[i];
			                $(row.site_info).each(function(i,n){
			                	html += ''+ n.x +'排'+ n.y +'座&nbsp;'
			                })
			           var pay_btn_txt = "查看详情";
			           if(info[i].status == "等待支付"){
			        	   pay_btn_txt = "去支付"
			           }
			                
			           html += '</div>'+
			            '<div class="col-sm-3" style="line-height: 150px;color: #DE4C41">'+
			                '¥'+info[i].price+
			            '</div>'+
			            '<div class="col-sm-2" style="line-height: 150px;">'+
			                info[i].status+
			            '</div>'+
			            '<div class="col-sm-2" style="line-height: 150px;">'+
			                '<button class="btn btn-sm buy-btn" order_id="'+info[i].orderId+'">'+pay_btn_txt+'</button>'+
			            '</div>'+
			        '</div>'+
			    '</div>'
        	}
        	
        	// 拼装分页表识别
        	html += '<br/><nav aria-label="Page navigation example"><ul class="pagination">'
        	var	pagination_css = ""
        	for(i=0;i<data.pageCount;i++){
        		if(i==data.pageNo){
        			var	pagination_css = "color:#DE4C41"
        		}
        		html += '<li class="page-item"><span class="page-link pagination_css">'+ (i+1) +'</a></li>'
        	}
        	html += '</ul></nav>'
        	
        	$("#right-block").html(html);
        })
    }



</script>
</html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>影片信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/moviehouse/movieHouse/">影厅信息列表</a></li>
		<shiro:hasPermission name="moviehouse:movieHouse:edit"><li><a href="${ctx}/moviehouse/movieHouse/form">影厅信息添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="movieHouse" action="${ctx}/moviehouse/movieHouse/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>影厅名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>编号：</label>
				<form:input path="number" htmlEscape="false" maxlength="20" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>影厅名称</th>
				<th>编号</th>
				<shiro:hasPermission name="moviehouse:movieHouse:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="movieHouse">
			<tr>
				<td><a href="${ctx}/moviehouse/movieHouse/form?id=${movieHouse.id}">
					${movieHouse.name}
				</a></td>
				<td>
					${movieHouse.number}
				</td>
				<shiro:hasPermission name="moviehouse:movieHouse:edit"><td>
    				<a href="${ctx}/moviehouse/movieHouse/form?id=${movieHouse.id}">修改</a>
					<a href="${ctx}/moviehouse/movieHouse/delete?id=${movieHouse.id}" onclick="return confirmx('确认要删除该影片信息吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>
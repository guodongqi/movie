<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>影院用户管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mvuser/mvuser/">影院用户列表</a></li>
		<shiro:hasPermission name="mvuser:mvuser:edit"><li><a href="${ctx}/mvuser/mvuser/form">影院用户添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="mvuser" action="${ctx}/mvuser/mvuser/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>用户昵称：</label>
				<form:input path="nickName" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>手机号：</label>
				<form:input path="phone" htmlEscape="false" maxlength="11" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>用户昵称</th>
				<th>手机号</th>
				<shiro:hasPermission name="mvuser:mvuser:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="mvuser">
			<tr>
				<td><a href="${ctx}/mvuser/mvuser/form?id=${mvuser.id}">
					${mvuser.nickName}
				</a></td>
				<td>
					${mvuser.phone}
				</td>
				<shiro:hasPermission name="mvuser:mvuser:edit"><td>
    				<a href="${ctx}/mvuser/mvuser/form?id=${mvuser.id}">修改</a>
					<a href="${ctx}/mvuser/mvuser/delete?id=${mvuser.id}" onclick="return confirmx('确认要删除该影院用户吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>